//
//  Category.swift
//  Super Todo List
//
//  Created by Andres Alderete on 2019-05-07.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name : String = ""
    @objc dynamic var color : String = ""
    let items = List<Item>()
}
