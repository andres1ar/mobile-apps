//
//  Item.swift
//  Super Todo List
//
//  Created by Andres Alderete on 2019-05-07.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    @objc dynamic var title : String = ""
    @objc dynamic var done : Bool = false
    @objc dynamic var dateCreated : Date?
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
