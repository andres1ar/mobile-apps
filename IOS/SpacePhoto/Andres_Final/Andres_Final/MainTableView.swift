//
//  ViewController.swift
//  Andres_Final
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import UIKit

class MainTableView: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    let itemController = ItemController()
    
    var flickItems = [Item]()
    var offlineList = [Item]()
    
    let url = URL(string: "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1")!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Table delegates
        tableView.delegate = self
        tableView.dataSource = self
        
        //loads items to array
        loadItems()
        
    }
    
    func loadItems(){
        itemController.fetchItems(url: url) { (items) in
            self.flickItems = items!
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    func updateUI(with items: [Item]){
        flickItems = items
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    @IBAction func saveButtonPressed(_ sender: UIBarButtonItem) {
        let propertyListEncoder = PropertyListEncoder()
        let encodedItemsArray = try? propertyListEncoder.encode(offlineList)
        try? encodedItemsArray?.write(to: Item.archiveURL, options: .noFileProtection)
    }
    
    @IBAction func modeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            //Disable save and edit buttons
            editButton.isEnabled = false
            saveButton.isEnabled = false
            loadItems()
            updateUI(with: flickItems)
            return
        case 1:
            //Load list from the file, if exists
            offlineList = loadOfflineListFromFile()
            
            //If the offline list is empty, copies the online one
            if offlineList.isEmpty{
                offlineList = flickItems}
            
            //Enables the buttons for offline list
            editButton.isEnabled = true
            saveButton.isEnabled = true
            
            updateUI(with: offlineList)
            
            return
            
        default:
            return
        }
    }
    
    @IBAction func editButtonPressed(_ sender: UIBarButtonItem) {
        let tableViewEditingMode = tableView.isEditing
        tableView.setEditing(!tableViewEditingMode, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete //.insert or .none
    }
    
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            offlineList.remove(at: indexPath.row)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .automatic)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }

    
    // Override to support rearranging the table view.
    func tableView(_ tableView: UITableView,
                   moveRowAt fromIndexPath: IndexPath,
                   to: IndexPath) {
        
        let movedItem = offlineList.remove(at: fromIndexPath.row)
        offlineList.insert(movedItem, at: to.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    @IBAction func saveList(_ sender: UIBarButtonItem) {
        let propertyListEncoder = PropertyListEncoder()
        let encodedItemsArray = try? propertyListEncoder.encode(offlineList)
        try? encodedItemsArray?.write(to: Item.archiveURL, options: .noFileProtection)
    }
    
    func loadOfflineListFromFile() -> [Item] {
        let propertyListDecoder = PropertyListDecoder()
        if let retrievedItemsData = try? Data(contentsOf: Item.archiveURL), let decodedItems = try? propertyListDecoder.decode(Array<Item>.self, from: retrievedItemsData){
            return decodedItems
        }
        return offlineList
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flickItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? ItemTableViewCell else {
            return UITableViewCell()
            
        }
        let item = flickItems[indexPath.row]
        cell.authorLabel.text = item.author
        cell.dateLabel.text = item.date_taken
        let data = try? Data(contentsOf: item.image.m)
        cell.imgView.image = UIImage(data: data!)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewDetail"{
            let indexPath = tableView.indexPathForSelectedRow!
            let item = flickItems[indexPath.row]
            let detailTableVC = segue.destination as! DetailTableViewController
            detailTableVC.item = item
        }
    }
    
}

