//
//  ItemController.swift
//  Andres_Final
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import Foundation
struct ItemController {
    
    func fetchItems(url: URL, completion:
        @escaping ([Item]?) -> Void) {
        
        let task = URLSession.shared.dataTask(with: url) { (data,
            response, error) in
            let decoder = JSONDecoder()
            if let data = data,
                let items = try?
                    decoder.decode(Items.self, from: data) {
                completion(items.items)
            } else {
                print("Either no data was returned, or data was not serialized.")
                completion(nil)
                return
            }
        }
        task.resume()
    }
    
}
