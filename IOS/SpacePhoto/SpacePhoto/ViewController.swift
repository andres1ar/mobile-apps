//
//  ViewController.swift
//  SpacePhoto
//
//  Created by mobileapps on 2019-02-12.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    
    let photoInfoController = PhotoInfoController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        firstLabel.text = ""
        secondLabel.text = ""
        photoInfoController.fetchPhotoInfo { (photoInfo) in
            if let photoInfo = photoInfo {
                self.updateUI(with: photoInfo)
                self.title = photoInfo.title
                self.firstLabel.text = photoInfo.description
                if let copyright = photoInfo.copyright {
                    self.secondLabel.text = "Copyright \(copyright)"
                } else {
                    self.secondLabel.isHidden = true
                }
            }
        }
    }
    
    func updateUI(with photoInfo: PhotoInfo) {
        guard let url = photoInfo.url.withHTTPS() else { return }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data,
                let image = UIImage(data: data) {
                DispatchQueue.main.sync {
                    self.title = photoInfo.title
                    self.firstLabel.text = photoInfo.description
                    
                    if let copyright = photoInfo.copyright {
                        self.secondLabel.text = "Copyright \(copyright)"
                    }else {
                        self.secondLabel.isHidden = true
                    }
                    self.image.image = image
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
        }
        task.resume()
    }
}

