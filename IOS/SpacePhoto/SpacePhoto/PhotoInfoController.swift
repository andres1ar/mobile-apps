//
//  PhotoInfoController.swift
//  SpacePhoto
//
//  Created by mobileapps on 2019-02-12.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import Foundation

struct PhotoInfoController {
    
    func fetchPhotoInfo(completion: @escaping (PhotoInfo?) -> Void) {
        let query: [String:String] = ["api_key": "WmKDwTvO4IkzyqaIf2FvurBKroILMEZmSbyd281a"]
        let baseURL = URL(string: "https://api.nasa.gov/planetary/apod")!
        let url = baseURL.withQueries(query)!
        print(url)
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            let jsonDecoder = JSONDecoder()
            if let data = data,
                let photoInfo = try? jsonDecoder.decode(PhotoInfo.self, from: data) {
                completion(photoInfo)
            } else {
                print("Either no data was returned, or data was not properly decoded.")
                completion(nil)
            }
        }
        task.resume()
    }
}
//https://api.nasa.gov/planetary/apod?api_key=WmKDwTvO4IkzyqaIf2FvurBKroILMEZmSbyd281a
