//
//  JSON.swift
//  Andres_Final
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import Foundation

let json =
    """
{
"title": "Uploads from everyone",
"link": "https:\/\/www.flickr.com\/photos\/",
"description": "",
"modified": "2019-02-19T14:14:53Z",
"generator": "https:\/\/www.flickr.com",
"items": [
{
"title": "IMG_037846\u62f7\u8c9dC",
"link": "https:\/\/www.flickr.com\/photos\/36141325@N03\/32204048947\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7824\/32204048947_48d2ebebbc_m.jpg"},
"date_taken": "2018-10-20T21:25:28-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/36141325@N03\/\">\u62b1\u6b49\u4f60\u8981\u627e\u7684\u4eba\u4e0d\u5728<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/36141325@N03\/32204048947\/\" title=\"IMG_037846\u62f7\u8c9dC\"><img src=\"https:\/\/farm8.staticflickr.com\/7824\/32204048947_48d2ebebbc_m.jpg\" width=\"240\" height=\"135\" alt=\"IMG_037846\u62f7\u8c9dC\" \/><\/a><\/p> ",
"published": "2019-02-19T14:14:53Z",
"author": "nobody@flickr.com (\"\u62b1\u6b49\u4f60\u8981\u627e\u7684\u4eba\u4e0d\u5728\")",
"author_id": "36141325@N03",
"tags": "even"
},
{
"title": "8b",
"link": "https:\/\/www.flickr.com\/photos\/142526478@N04\/33270726828\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7918\/33270726828_0c881dd9b6_m.jpg"},
"date_taken": "2019-02-11T19:19:23-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/142526478@N04\/\">allisondooley17<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/142526478@N04\/33270726828\/\" title=\"8b\"><img src=\"https:\/\/farm8.staticflickr.com\/7918\/33270726828_0c881dd9b6_m.jpg\" width=\"160\" height=\"240\" alt=\"8b\" \/><\/a><\/p> ",
"published": "2019-02-19T14:14:36Z",
"author": "nobody@flickr.com (\"allisondooley17\")",
"author_id": "142526478@N04",
"tags": ""
},
{
"title": " ",
"link": "https:\/\/www.flickr.com\/photos\/161468956@N06\/33270734148\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7873\/33270734148_443896f2e0_m.jpg"},
"date_taken": "2019-02-19T13:25:19-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/161468956@N06\/\">nhatyenbui89<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/161468956@N06\/33270734148\/\" title=\" \"><img src=\"https:\/\/farm8.staticflickr.com\/7873\/33270734148_443896f2e0_m.jpg\" width=\"240\" height=\"234\" alt=\" \" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:15Z",
"author": "nobody@flickr.com (\"nhatyenbui89\")",
"author_id": "161468956@N06",
"tags": ""
},
{
"title": "Russian chocolate eclairs",
"link": "https:\/\/www.flickr.com\/photos\/memorydrone\/33270734578\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7851\/33270734578_6b6f542112_m.jpg"},
"date_taken": "2019-02-17T11:49:48-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/memorydrone\/\">Memory Drone<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/memorydrone\/33270734578\/\" title=\"Russian chocolate eclairs\"><img src=\"https:\/\/farm8.staticflickr.com\/7851\/33270734578_6b6f542112_m.jpg\" width=\"240\" height=\"154\" alt=\"Russian chocolate eclairs\" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:18Z",
"author": "nobody@flickr.com (\"Memory Drone\")",
"author_id": "165796784@N07",
"tags": ""
},
{
"title": "_MG_9366",
"link": "https:\/\/www.flickr.com\/photos\/141191772@N06\/33270736648\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7808\/33270736648_5f83cce189_m.jpg"},
"date_taken": "2019-02-19T11:10:37-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/141191772@N06\/\">H\u00e0 Xu\u00e2n Nh\u00e2n<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/141191772@N06\/33270736648\/\" title=\"_MG_9366\"><img src=\"https:\/\/farm8.staticflickr.com\/7808\/33270736648_5f83cce189_m.jpg\" width=\"240\" height=\"160\" alt=\"_MG_9366\" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:30Z",
"author": "nobody@flickr.com (\"H\u00e0 Xu\u00e2n Nh\u00e2n\")",
"author_id": "141191772@N06",
"tags": ""
},
{
"title": "2011-01-28 Rim 040.jpg",
"link": "https:\/\/www.flickr.com\/photos\/95991403@N02\/33270737548\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7867\/33270737548_78095cef8f_m.jpg"},
"date_taken": "2011-01-28T16:34:39-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/95991403@N02\/\">jezekp<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/95991403@N02\/33270737548\/\" title=\"2011-01-28 Rim 040.jpg\"><img src=\"https:\/\/farm8.staticflickr.com\/7867\/33270737548_78095cef8f_m.jpg\" width=\"180\" height=\"240\" alt=\"2011-01-28 Rim 040.jpg\" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:33Z",
"author": "nobody@flickr.com (\"jezekp\")",
"author_id": "95991403@N02",
"tags": ""
},
{
"title": "20190219091225ch01",
"link": "https:\/\/www.flickr.com\/photos\/10194419@N00\/40181000513\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7858\/40181000513_04edfe6b27_m.jpg"},
"date_taken": "2019-02-19T09:14:47-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/10194419@N00\/\">Boot Seem<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/10194419@N00\/40181000513\/\" title=\"20190219091225ch01\"><img src=\"https:\/\/farm8.staticflickr.com\/7858\/40181000513_04edfe6b27_m.jpg\" width=\"240\" height=\"135\" alt=\"20190219091225ch01\" \/><\/a><\/p> <p>Alarm Event: Motion Detect<br \/> Alarm Input Channel: 1<br \/> Alarm Start Time(D\/M\/Y H:M:S): 19\/02\/2019 09:12:25<br \/> Alarm Device Name: AMC046CC_31B4BF<br \/> Alarm Name:<br \/> IP Address: 192.168.1.251<\/p>",
"published": "2019-02-19T14:14:47Z",
"author": "nobody@flickr.com (\"Boot Seem\")",
"author_id": "10194419@N00",
"tags": "olden2"
},
{
"title": " ",
"link": "https:\/\/www.flickr.com\/photos\/brinbring\/40181002083\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7808\/40181002083_c109a8c26b_m.jpg"},
"date_taken": "2019-02-19T22:00:43-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/brinbring\/\">Garfield_Lin<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/brinbring\/40181002083\/\" title=\" \"><img src=\"https:\/\/farm8.staticflickr.com\/7808\/40181002083_c109a8c26b_m.jpg\" width=\"160\" height=\"240\" alt=\" \" \/><\/a><\/p> ",
"published": "2019-02-19T14:14:59Z",
"author": "nobody@flickr.com (\"Garfield_Lin\")",
"author_id": "84496998@N00",
"tags": ""
},
{
"title": "ASDA Ellesmere Port Jolly Roger Standard Thomas and Friends Kiddie Ride",
"link": "https:\/\/www.flickr.com\/photos\/ajthefangirl\/40181002343\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7841\/40181002343_5e4f8336f8_m.jpg"},
"date_taken": "2019-02-01T08:20:17-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/ajthefangirl\/\">amberthefangirl<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/ajthefangirl\/40181002343\/\" title=\"ASDA Ellesmere Port Jolly Roger Standard Thomas and Friends Kiddie Ride\"><img src=\"https:\/\/farm8.staticflickr.com\/7841\/40181002343_5e4f8336f8_m.jpg\" width=\"180\" height=\"240\" alt=\"ASDA Ellesmere Port Jolly Roger Standard Thomas and Friends Kiddie Ride\" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:01Z",
"author": "nobody@flickr.com (\"amberthefangirl\")",
"author_id": "145535224@N08",
"tags": ""
},
{
"title": "47812",
"link": "https:\/\/www.flickr.com\/photos\/166751408@N08\/46231798855\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7804\/46231798855_52375b43ae_m.jpg"},
"date_taken": "2019-02-19T06:15:03-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/166751408@N08\/\">lewiswarman26<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/166751408@N08\/46231798855\/\" title=\"47812\"><img src=\"https:\/\/farm8.staticflickr.com\/7804\/46231798855_52375b43ae_m.jpg\" width=\"240\" height=\"180\" alt=\"47812\" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:03Z",
"author": "nobody@flickr.com (\"lewiswarman26\")",
"author_id": "166751408@N08",
"tags": ""
},
{
"title": "Shapes and curves #quebec #quebeccity #museums #mnbaq #bw #bnw #bnw_city #bnw_one #bnw_dark #bnw_of_our_world #blackandwhite #bnwlovers #bnw_life #bnw_demand #bnw_society #bnw_zone #bnw_captures #bnw_demand #snapseed #vty_2019 #igersquebec #igers #creativ",
"link": "https:\/\/www.flickr.com\/photos\/vannara\/46422558934\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7876\/46422558934_0dc3598eac_m.jpg"},
"date_taken": "2019-02-19T08:14:57-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/vannara\/\">Vannara<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/vannara\/46422558934\/\" title=\"Shapes and curves #quebec #quebeccity #museums #mnbaq #bw #bnw #bnw_city #bnw_one #bnw_dark #bnw_of_our_world #blackandwhite #bnwlovers #bnw_life #bnw_demand #bnw_society #bnw_zone #bnw_captures #bnw_demand #snapseed #vty_2019 #igersquebec #igers #creativ\"><img src=\"https:\/\/farm8.staticflickr.com\/7876\/46422558934_0dc3598eac_m.jpg\" width=\"240\" height=\"240\" alt=\"Shapes and curves #quebec #quebeccity #museums #mnbaq #bw #bnw #bnw_city #bnw_one #bnw_dark #bnw_of_our_world #blackandwhite #bnwlovers #bnw_life #bnw_demand #bnw_society #bnw_zone #bnw_captures #bnw_demand #snapseed #vty_2019 #igersquebec #igers #creativ\" \/><\/a><\/p> <p>View on Instagram <a href=\"http:\/\/bit.ly\/2Iok7rh\" rel=\"noreferrer nofollow\">bit.ly\/2Iok7rh<\/a><\/p>",
"published": "2019-02-19T14:14:57Z",
"author": "nobody@flickr.com (\"Vannara\")",
"author_id": "33563297@N03",
"tags": "instagram ifttt"
},
{
"title": "DSC01629",
"link": "https:\/\/www.flickr.com\/photos\/141073700@N08\/46422561904\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7854\/46422561904_fef6a785d3_m.jpg"},
"date_taken": "2019-02-17T11:02:57-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/141073700@N08\/\">wayway3131<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/141073700@N08\/46422561904\/\" title=\"DSC01629\"><img src=\"https:\/\/farm8.staticflickr.com\/7854\/46422561904_fef6a785d3_m.jpg\" width=\"160\" height=\"240\" alt=\"DSC01629\" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:11Z",
"author": "nobody@flickr.com (\"wayway3131\")",
"author_id": "141073700@N08",
"tags": ""
},
{
"title": "#freestyle #abstractart #digitalart #modernart #design #contemporaryart #COLOR #accessories #unicit\u00e0 #creativit\u00e0 #raffreefly_vendita_opere #design #interiodesign #visualart #visualdesign #visualeffects #visualartist #wallpaper #cartadaparatiartistica #car",
"link": "https:\/\/www.flickr.com\/photos\/raffreefly\/47093777362\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7897\/47093777362_6c0f5c47b5_m.jpg"},
"date_taken": "2019-02-19T02:35:52-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/raffreefly\/\">raffreefly<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/raffreefly\/47093777362\/\" title=\"#freestyle #abstractart #digitalart #modernart #design #contemporaryart #COLOR #accessories #unicit\u00e0 #creativit\u00e0 #raffreefly_vendita_opere #design #interiodesign #visualart #visualdesign #visualeffects #visualartist #wallpaper #cartadaparatiartistica #car\"><img src=\"https:\/\/farm8.staticflickr.com\/7897\/47093777362_6c0f5c47b5_m.jpg\" width=\"237\" height=\"240\" alt=\"#freestyle #abstractart #digitalart #modernart #design #contemporaryart #COLOR #accessories #unicit\u00e0 #creativit\u00e0 #raffreefly_vendita_opere #design #interiodesign #visualart #visualdesign #visualeffects #visualartist #wallpaper #cartadaparatiartistica #car\" \/><\/a><\/p> ",
"published": "2019-02-19T14:14:29Z",
"author": "nobody@flickr.com (\"raffreefly\")",
"author_id": "124854277@N02",
"tags": "visualart freestyle abstractart cartedaparati woman wallpaper unicit\u00e0 accessories raffreeflyvenditaopere cartadaparatiartistica modernart visualdesign visualeffects visualartist digitalart color design contemporaryart creativit\u00e0 interiodesign"
},
{
"title": "Today\u2019s bicycle commute photo: I\u2019ll never have that recipe again... #bicycle #commute #austin",
"link": "https:\/\/www.flickr.com\/photos\/s-t-e-v-e-n\/47093780032\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7808\/47093780032_2d1b46fd6d_m.jpg"},
"date_taken": "2019-02-19T08:14:43-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/s-t-e-v-e-n\/\">S-t-e-v-e-n<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/s-t-e-v-e-n\/47093780032\/\" title=\"Today\u2019s bicycle commute photo: I\u2019ll never have that recipe again... #bicycle #commute #austin\"><img src=\"https:\/\/farm8.staticflickr.com\/7808\/47093780032_2d1b46fd6d_m.jpg\" width=\"240\" height=\"240\" alt=\"Today\u2019s bicycle commute photo: I\u2019ll never have that recipe again... #bicycle #commute #austin\" \/><\/a><\/p> <p>via Instagram <a href=\"http:\/\/bit.ly\/2DRr3aO\" rel=\"noreferrer nofollow\">bit.ly\/2DRr3aO<\/a><\/p>",
"published": "2019-02-19T14:14:43Z",
"author": "nobody@flickr.com (\"S-t-e-v-e-n\")",
"author_id": "34813830@N00",
"tags": "ifttt instagram bicycle commute austin"
},
{
"title": " ",
"link": "https:\/\/www.flickr.com\/photos\/49406930@N08\/47093785442\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7870\/47093785442_c32a498ba3_m.jpg"},
"date_taken": "2019-02-19T06:15:12-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/49406930@N08\/\">jasonpapa<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/49406930@N08\/47093785442\/\" title=\" \"><img src=\"https:\/\/farm8.staticflickr.com\/7870\/47093785442_c32a498ba3_m.jpg\" width=\"240\" height=\"212\" alt=\" \" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:12Z",
"author": "nobody@flickr.com (\"jasonpapa\")",
"author_id": "49406930@N08",
"tags": ""
},
{
"title": "Britt Andros? Who is Britt Andros? This is Britt!",
"link": "https:\/\/www.flickr.com\/photos\/brittandros\/47093785622\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7846\/47093785622_2e74a61a5e_m.jpg"},
"date_taken": "2019-02-19T06:15:13-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/brittandros\/\">brittandros<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/brittandros\/47093785622\/\" title=\"Britt Andros? Who is Britt Andros? This is Britt!\"><img src=\"https:\/\/farm8.staticflickr.com\/7846\/47093785622_2e74a61a5e_m.jpg\" width=\"240\" height=\"156\" alt=\"Britt Andros? Who is Britt Andros? This is Britt!\" \/><\/a><\/p> <p>I know Britt Andros<\/p>",
"published": "2019-02-19T14:15:13Z",
"author": "nobody@flickr.com (\"brittandros\")",
"author_id": "141502288@N05",
"tags": "britt andros who is"
},
{
"title": "DSC_0158",
"link": "https:\/\/www.flickr.com\/photos\/164172785@N03\/47093789102\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7896\/47093789102_c04ca7e9af_m.jpg"},
"date_taken": "2019-02-15T18:16:08-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/164172785@N03\/\">maysingh123456<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/164172785@N03\/47093789102\/\" title=\"DSC_0158\"><img src=\"https:\/\/farm8.staticflickr.com\/7896\/47093789102_c04ca7e9af_m.jpg\" width=\"240\" height=\"160\" alt=\"DSC_0158\" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:32Z",
"author": "nobody@flickr.com (\"maysingh123456\")",
"author_id": "164172785@N03",
"tags": ""
},
{
"title": "monique",
"link": "https:\/\/www.flickr.com\/photos\/127852634@N05\/47146041041\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7823\/47146041041_e05ef4a54b_m.jpg"},
"date_taken": "2019-02-19T15:15:21-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/127852634@N05\/\">Portrait Gallery of the Golden Age<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/127852634@N05\/47146041041\/\" title=\"monique\"><img src=\"https:\/\/farm8.staticflickr.com\/7823\/47146041041_e05ef4a54b_m.jpg\" width=\"240\" height=\"135\" alt=\"monique\" \/><\/a><\/p> <p>Here is my picture taken at Hermitage &quot;20190219151329&quot;<\/p>",
"published": "2019-02-19T14:15:21Z",
"author": "nobody@flickr.com (\"Portrait Gallery of the Golden Age\")",
"author_id": "127852634@N05",
"tags": "22102014 monique"
},
{
"title": "DSC_5230.jpg",
"link": "https:\/\/www.flickr.com\/photos\/43256651@N07\/47146042281\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7864\/47146042281_5a5e905417_m.jpg"},
"date_taken": "2019-02-17T11:35:32-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/43256651@N07\/\">macmmh<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/43256651@N07\/47146042281\/\" title=\"DSC_5230.jpg\"><img src=\"https:\/\/farm8.staticflickr.com\/7864\/47146042281_5a5e905417_m.jpg\" width=\"240\" height=\"160\" alt=\"DSC_5230.jpg\" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:33Z",
"author": "nobody@flickr.com (\"macmmh\")",
"author_id": "43256651@N07",
"tags": ""
},
{
"title": "Impor",
"link": "https:\/\/www.flickr.com\/photos\/160255495@N04\/47146042371\/",
"media": {"m":"https:\/\/farm8.staticflickr.com\/7857\/47146042371_7a8b12d945_m.jpg"},
"date_taken": "2019-02-19T06:15:35-08:00",
"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/160255495@N04\/\">andaju<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/160255495@N04\/47146042371\/\" title=\"Impor\"><img src=\"https:\/\/farm8.staticflickr.com\/7857\/47146042371_7a8b12d945_m.jpg\" width=\"240\" height=\"180\" alt=\"Impor\" \/><\/a><\/p> ",
"published": "2019-02-19T14:15:35Z",
"author": "nobody@flickr.com (\"andaju\")",
"author_id": "160255495@N04",
"tags": ""
}
]
}
""".data(using: .utf8)!
