//
//  Item.swift
//  Andres_Final
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import Foundation

struct Items: Codable {
    let items: [Item]
}

struct Media: Codable{
    var m: URL
    
    enum CodingKeys: String, CodingKey {
        // Mapping between enum CodingKeys with JSON keys or accessing JSON keys
        case m = "m"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        m = try values.decode(URL.self, forKey: CodingKeys.m)
    }
}
struct Item: Codable {
    
    var author: String
    var date_taken: String
    var description: String
    var published: String
    var tags: String
    var image: Media
    
    enum CodingKeys: String, CodingKey {
        // Mapping between enum CodingKeys with JSON keys or accessing JSON keys
        case author = "author"
        case date_taken = "date_taken"
        case description = "description"
        case published = "published"
        case tags = "tags"
        case image = "media"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        // Mapping betwen instance variables and CodingKeys cases
        author = try values.decode(String.self, forKey: CodingKeys.author)
        date_taken = try values.decode(String.self, forKey: CodingKeys.date_taken)
        description = try values.decode(String.self, forKey: CodingKeys.description)
        published = try values.decode(String.self, forKey: CodingKeys.published)
        tags = try values.decode(String.self, forKey: CodingKeys.tags)
        image = try values.decode(Media.self, forKey: CodingKeys.image)
    }
    
    static let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    static let archiveURL = documentDirectory.appendingPathComponent("items").appendingPathExtension("me")
    
}

