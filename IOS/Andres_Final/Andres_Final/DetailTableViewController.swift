//
//  DetailTableViewController.swift
//  Andres_Final
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {
    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var publishedLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    
    
    var item: Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let item = item {
            descriptionLabel.text = item.description
            publishedLabel.text = item.published
            tagsLabel.text = item.tags
            let data = try? Data(contentsOf: item.image.m)
            imgView.image = UIImage(data: data!)
            
        }
    }

    @IBAction func shareButton(_ sender: UIBarButtonItem) {
         if let item = item {
        let data = try? Data(contentsOf: item.image.m)
        let image = UIImage(data: data!)
            let info = """
            \(item.description)
            \(item.published)
            \(item.tags)
            
            """
            
        let activityController = UIActivityViewController(activityItems: [info, image!], applicationActivities: nil)
        activityController.popoverPresentationController?.barButtonItem = sender
        present(activityController, animated: true, completion: nil)
        }
    }
   
}
