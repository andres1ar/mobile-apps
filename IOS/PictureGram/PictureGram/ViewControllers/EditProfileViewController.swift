//
//  EditProfileViewController.swift
//  PictureGram
//
//  Created by Andres Alderete on 2019-03-27.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage
import ProgressHUD

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var displayNameText: UITextField!
    @IBOutlet weak var bioText: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    
    var selectedImage: UIImage?
    
    var databaseRef: DatabaseReference!
    var storageRef: StorageReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadProfileData()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleSelectProfileImageView))
        profileImageView.addGestureRecognizer(tapGesture)
        profileImageView.isUserInteractionEnabled = true
//        signUpButton.isEnabled = false
//        handleTextField()
        
    }
    
    @objc func handleSelectProfileImageView(){
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        present(pickerController, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func loadProfileData(){
        
         databaseRef = Database.database().reference()
         storageRef = Storage.storage().reference()
        
        if let userID = Auth.auth().currentUser?.uid{
            databaseRef.child("users").child(userID).observe(.value) { (snapshot) in
                
                let values = snapshot.value as? NSDictionary
                
                if let profileImageUrl = values?["profileImageUrl"] as? String {
                    self.profileImageView.sd_setImage(with: URL(string: profileImageUrl))
                }
                self.usernameText.text = values?["username"] as? String
                self.displayNameText.text = values?["display"] as? String
                self.bioText.text = values?["bio"] as? String
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveProfileData(_ sender: Any) {
        updateUserProfile()
    }
    
    func updateUserProfile(){
        if let userID = Auth.auth().currentUser?.uid{
            let storageItem = storageRef.child("profile_image").child(userID)
            guard let image = profileImageView.image else {
                return
            }
            if let newImage = image.pngData(){
                storageItem.putData(newImage, metadata: nil) { (metadata, error) in
                    if error != nil{
                        print(error!.localizedDescription)
                        return
                    }
                    storageItem.downloadURL(completion: { (url, error) in
                        if error != nil {
                            print(error!.localizedDescription)
                            return
                        }
                        if let profileImageUrl = url?.absoluteString {
                            guard let newUserName = self.usernameText.text else {return}
                            guard let newDisplayName = self.displayNameText.text else {return}
                            guard let newBioText = self.bioText.text else {return}
                            
                            let newValuesForProfile = ["profileImageUrl": profileImageUrl,
                                                       "username": newUserName,
                                                       "display": newDisplayName,
                                                       "bio": newBioText]
                            
                            self.databaseRef.child("users").child(userID).updateChildValues(newValuesForProfile, withCompletionBlock: { (error, ref) in
                                if error != nil{
                                    print(error!.localizedDescription)
                                    return
                                }
                                ProgressHUD.showSuccess("Profile Updated!")
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                    })
                }
            }
        }
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            selectedImage = image
            profileImageView.image = image
        }
        dismiss(animated: true, completion: nil)
    }
}
