//
//  FollowRequestController.swift
//  PictureGram
//
//  Created by Andres Alderete on 2019-03-29.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class FollowRequestController{
    
    func requestToFollowUser(userWeWantToFollow: String, dbRef: DatabaseReference){
        weak var weakSelf = self
        if let currentUser = Auth.auth().currentUser?.uid{
            let myDataRef = dbRef.child("Users").child(currentUser)
        
            myDataRef.observeSingleEvent(of: .value) { (snapshot) in
                let myData = snapshot.value as! [String: String]
                
                let userFollowRequestListRef = dbRef.child("FollowerRequest").child(userWeWantToFollow).child(currentUser)
                
                userFollowRequestListRef.updateChildValues(myData)
                weakSelf?.addUserToFollowingList(user: userWeWantToFollow, status: "pending", currentUser: currentUser)
                print("new request sent")
            }
        }
    }
    fileprivate func addUserToFollowingList(user: String, status: String, currentUser: String){
        let myFollowingsRef = Database.database().reference().child("Following").child(currentUser)
        myFollowingsRef.updateChildValues([user:status])
    }
}
