//
//  FilterCollectionViewCell.swift
//  PictureGram
//
//  Created by Andres Alderete on 2019-03-30.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation
import UIKit

class FilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var filterPhoto: UIImageView!
}
