//
//  Comment.swift
//  PictureGram
//
//  Created by Andres Alderete on 2019-03-30.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation

class Comment {
    var commentText: String?
    var uid: String?
}

extension Comment {
    static func transformComment(dict: [String: Any]) -> Comment {
        let comment = Comment()
        comment.commentText = dict["commentText"] as? String
        comment.uid = dict["uid"] as? String
        return comment
    }
}
