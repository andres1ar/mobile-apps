//
//  ViewController.swift
//  Movies
//
//  Created by Andres Alderete on 2019-02-08.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var movies = [
        Movie(title: "Harry Potter and the Philosopher's Stone", director: "Chris Columbus", picture: "harrypotter1"),
        Movie(title: "Harry Potter and the Chamber of Secrets", director: "Chris Columbus", picture: "harrypotter2"),
        Movie(title: "Harry Potter and the Prisoner of Azkaban", director: "Alfonso Cuarón", picture: "harrypotter3"),
        Movie(title: "Harry Potter and the Goblet of Fire", director: "Mike Newell", picture: "harrypotter4"),
        Movie(title: "Harry Potter and the Order of the Phoenix", director: "David Yates", picture: "harrypotter5"),
        Movie(title: "Harry Potter and the Half-Blood Prince", director: "David Yates", picture: "harrypotter6"),
        Movie(title: "Harry Potter and the Deathly Hallows – Part 1", director: "David Yates", picture: "harrypotter7"),
        Movie(title: "Harry Potter and the Deathly Hallows – Part 2", director: "David Yates", picture: "harrypotter8"),
        ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // DataSource and Delegate
        tableView.dataSource = self
        tableView.delegate = self
        
        //  Automatic cell height
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44.0
    }
    
    @IBAction func editButtonTapped(_ sender: UIBarButtonItem) {
        let tableViewEditingMode = tableView.isEditing
        tableView.setEditing(!tableViewEditingMode, animated: true)
    }
    
    func tableView(_ tableView: UITableView,
                            commit editingStyle: UITableViewCell.EditingStyle,
                            forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            movies.remove(at: indexPath.row)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .automatic)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    // Override to support rearranging the table view.
    func tableView(_ tableView: UITableView,
                            moveRowAt fromIndexPath: IndexPath,
                            to: IndexPath) {
        
        let movedMovie = movies.remove(at: fromIndexPath.row)
        movies.insert(movedMovie, at: to.row)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updateMovie"{
            let indexPath = tableView.indexPathForSelectedRow!
            let movie = movies[indexPath.row]
            
            let navVC = segue.destination as! UINavigationController
            let addEditMovieTableViewController = navVC.viewControllers.first as! AddEditMovieTableViewController
            
            addEditMovieTableViewController.movie = movie
        }
    }
    @IBAction func unwindToMovieTableView(segue: UIStoryboardSegue){
        
        guard segue.identifier == "saveUnwind" else {return}
        let sourceViewController = segue.source as! AddEditMovieTableViewController
        
        if let movie = sourceViewController.movie {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                movies[selectedIndexPath.row] = movie
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                let newIndexPath = IndexPath(row: movies.count, section: 0)
                movies.append(movie)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MovieTableViewCell
        cell.picture.image = UIImage(named: movies[indexPath.row].picture)
        cell.titleLabel.text = movies[indexPath.row].title
        cell.directorLabel.text = movies[indexPath.row].director
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}

extension ViewController: UITableViewDelegate {
    
}

