//
//  AddEditMovieTableViewController.swift
//  Movies
//
//  Created by Andres Alderete on 2019-02-08.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class AddEditMovieTableViewController: UITableViewController {

    @IBOutlet weak var moviePicture: UIImageView!
    @IBOutlet weak var movieTitleLabel: UITextField!
    @IBOutlet weak var movieDirectorLabel: UITextField!
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var movie: Movie?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let movie = movie {
            movieTitleLabel.text = movie.title
            movieDirectorLabel.text = movie.director
            moviePicture.image = UIImage(named: movie.picture)
            
        }
        updateSaveButtonState()
    }

    @IBAction func textEditingChanged(_ sender: UITextField) {
        updateSaveButtonState()
    }
    
    func updateSaveButtonState(){
        let titleText = movieTitleLabel.text ?? ""
        let directorText = movieDirectorLabel.text ?? ""
        
        
        saveButton.isEnabled = !titleText.isEmpty &&
            !directorText.isEmpty
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard segue.identifier == "saveUnwind" else {return}
        
        let title = movieTitleLabel.text ?? ""
        let director = movieDirectorLabel.text ?? ""
        let image = movie?.picture ?? ""
        
        movie = Movie(title: title, director: director, picture: image)
    }
   
}
