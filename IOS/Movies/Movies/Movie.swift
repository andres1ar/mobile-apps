//
//  Movie.swift
//  Movies
//
//  Created by Andres Alderete on 2019-02-08.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation
class Movie {
    var title: String
    var director: String
    var picture: String
    
    
    init(title: String, director: String, picture: String){
        self.title = title
        self.director = director
        self.picture = picture
    }
}
