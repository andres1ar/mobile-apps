//
//  Constants.swift
//  Open Thoughts
//
//  Created by Andres Alderete on 2019-09-05.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation

let THOUGHTS_REF = "thoughts"
let USERS_REF = "users"
let COMMENTS_REF = "comments"
let LIKED_BY_REF = "likedBy"

let CATEGORY = "category"
let NUM_COMMENTS = "numComments"
let NUM_LIKES = "numLikes"
let THOUGHT_TXT = "thoughtTxt"
let TIMESTAMP = "timestamp"
let USERNAME = "username"
let DATE_CREATED = "dateCreated"
let COMMENT_TXT = "commentTxt"
let USER_ID = "userId"
