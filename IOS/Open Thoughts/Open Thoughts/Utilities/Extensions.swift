//
//  Extensions.swift
//  Open Thoughts
//
//  Created by Andres Alderete on 2019-09-10.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

extension UIButton {
    
    //Function to wiggle animate a button
    //This could be use if a login fails, the button will wiggle
    func wiggle() {
        let wiggleAnim = CABasicAnimation(keyPath: "position")
        wiggleAnim.duration = 0.05
        wiggleAnim.repeatCount = 5
        wiggleAnim.autoreverses = true
        wiggleAnim.fromValue = CGPoint(x: self.center.x - 4.0, y: self.center.y)
        wiggleAnim.toValue = CGPoint(x: self.center.x + 4.0, y: self.center.y)
        layer.add(wiggleAnim, forKey: "position")
}
}
