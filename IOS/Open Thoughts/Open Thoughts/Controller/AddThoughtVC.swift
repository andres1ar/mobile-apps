//
//  AddThoughtVC.swift
//  Open Thoughts
//
//  Created by Andres Alderete on 2019-09-05.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Firebase

class AddThoughtVC: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var categorySegment: UISegmentedControl!
    @IBOutlet weak var thoughtTxt: UITextView!
    @IBOutlet weak var postBtn: UIButton!
    
    private var selectedCategory = ThoughtCategory.funny.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()
        thoughtTxt.delegate = self
    }
    
    func setupTextfields() {
        
        postBtn.layer.cornerRadius = 4
        thoughtTxt.layer.cornerRadius = 4
        thoughtTxt.text = "My random thought..."
        thoughtTxt.textColor = UIColor.lightGray
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = UIColor.darkGray
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if thoughtTxt.text == "" {
            thoughtTxt.text = "My random thought..."
            thoughtTxt.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func postBtnTapped(_ sender: Any) {
        //TODO: check empty fields before uploading
        let thoughtData: [String : Any] = [
            CATEGORY : selectedCategory,
            NUM_COMMENTS : 0,
            NUM_LIKES : 0,
            THOUGHT_TXT :  thoughtTxt.text!,
            TIMESTAMP : FieldValue.serverTimestamp(),
            USERNAME : Auth.auth().currentUser?.displayName ?? "" ,
            USER_ID : Auth.auth().currentUser?.uid ?? ""
        ]
        
        Firestore.firestore().collection(THOUGHTS_REF).addDocument(data: thoughtData) { (error) in
            if let error = error {
                debugPrint("Error adding document: \(error)")
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func categoryChanged(_ sender: Any) {
        
        switch categorySegment.selectedSegmentIndex {
        case 0:
            selectedCategory = ThoughtCategory.funny.rawValue
        case 1:
            selectedCategory = ThoughtCategory.serious.rawValue
        default:
            selectedCategory = ThoughtCategory.crazy.rawValue
        }
    }
    
    
}
