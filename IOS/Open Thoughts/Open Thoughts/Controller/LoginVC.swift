//
//  LoginVC.swift
//  Open Thoughts
//
//  Created by Andres Alderete on 2019-09-06.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import FirebaseAuth
import SVProgressHUD
import GoogleSignIn
import FBSDKLoginKit
import TwitterKit

class LoginVC: UIViewController, GIDSignInUIDelegate, LoginButtonDelegate {
    
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var createUserBtn: UIButton!
    @IBOutlet weak var googleSignIn: GIDSignInButton!
    @IBOutlet weak var facebookLoginBtn: FBLoginButton!
    @IBOutlet weak var twitterLoginBtn: TWTRLogInButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTxt.delegate = self
        passwordTxt.delegate = self
        
        setupView()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        facebookLoginBtn.delegate = self
        facebookLoginBtn.permissions = ["email"]
        
        let loginTwitterButton = TWTRLogInButton { (session, error) in
            if let error = error {
                debugPrint("could not login twitter", error)
            }
            if let session = session {
                let credential = TwitterAuthProvider.credential(withToken: session.authToken, secret: session.authTokenSecret)
                self.firebaseLogin(credential)
            }
        }
        loginTwitterButton.center.y = twitterLoginBtn.center.y - 5
        twitterLoginBtn.addSubview(loginTwitterButton)
        
        
        //        loginTwitterButton.center.x = twitterBtnView.center.x
        //        twitterBtnView.addSubview(loginTwitterButton)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user == nil {
                //No user
            } else {
                //User in, dismiss
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //MARK: Logins
    //Facebook
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if let error = error {
            debugPrint("Failed facebook login", error)
            return
        }
        let credential = FacebookAuthProvider.credential(withAccessToken: result?.token?.tokenString ?? "")
        firebaseLogin(credential)
    }
    
    //Firebase
    func firebaseLogin(_ credential: AuthCredential) {
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
                return
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        //Handle logout sequence
    }
    
    //TODO: Login automatically after creating an user
    
    func setupView() {
        loginBtn.layer.cornerRadius = 10
        createUserBtn.layer.cornerRadius = 10
    }
    
    @IBAction func loginBtnTapped(_ sender: Any) {
        guard let email = emailTxt.text, emailTxt.text != "",
            let password = passwordTxt.text, passwordTxt.text != "" else {
                //If any field is empty the button will wiggle to show that someting is missing
                loginBtn.wiggle()
                return
        }
        SVProgressHUD.show()
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let error = error {
                debugPrint("Error signing in: \(error)")
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: error.localizedDescription)
            } else {
                SVProgressHUD.dismiss()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

extension LoginVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
