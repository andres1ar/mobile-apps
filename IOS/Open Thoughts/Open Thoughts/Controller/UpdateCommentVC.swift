//
//  UpdateCommentVC.swift
//  Open Thoughts
//
//  Created by Andres Alderete on 2019-09-09.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Firebase

class UpdateCommentVC: UIViewController {
    
    @IBOutlet weak var commentTxt: UITextView!
    @IBOutlet weak var updateBtn: UIButton!
    
    //variable to hold the information about the comment to update
    var commentData: (comment: Comment, thought: Thought)!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        commentTxt.layer.cornerRadius = 10
        updateBtn.layer.cornerRadius = 10
        commentTxt.text = commentData.comment.commentTxt
    }
    
    
    
    @IBAction func updateTapped(_ sender: Any) {
        //Check that the field is not empty
        Firestore.firestore().collection(THOUGHTS_REF).document(commentData.thought.documentId)
            .collection(COMMENTS_REF).document(commentData.comment.documentId)
            .updateData([COMMENT_TXT : commentTxt.text!]) { (error) in
                if let error = error {
                    debugPrint("Unable to update comment: \(error.localizedDescription)")
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
        }
    }
    
}
