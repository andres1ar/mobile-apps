//
//  CreateUserVC.swift
//  Open Thoughts
//
//  Created by Andres Alderete on 2019-09-06.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import SVProgressHUD


class CreateUserVC: UIViewController {
    
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var createBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    func setupView() {
        createBtn.layer.cornerRadius = 10
        cancelBtn.layer.cornerRadius = 10
    }
    
    @IBAction func createTapped(_ sender: Any) {
        guard let email = emailTxt.text, emailTxt.text != "",
            let password = passwordTxt.text, passwordTxt.text != "",
            let username = usernameTxt.text, usernameTxt.text != "" else {
                createBtn.wiggle()
                return
        }
        
        SVProgressHUD.show()
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let error = error {
                debugPrint("Error creating user: \(error.localizedDescription)")
                self.dismissAndShowError(error)
            }
            
            let changeRequest = user?.user.createProfileChangeRequest()
            changeRequest?.displayName = username
            changeRequest?.commitChanges(completion: { (error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                    self.dismissAndShowError(error)
                }
            })
            
            guard let userId = user?.user.uid else { return }
            Firestore.firestore().collection(USERS_REF).document(userId).setData([
                USERNAME : username,
                DATE_CREATED : FieldValue.serverTimestamp()
                ], completion: { (error) in
                    if let error = error {
                        debugPrint(error.localizedDescription)
                        self.dismissAndShowError(error)
                    } else {
                        SVProgressHUD.showSuccess(withStatus: "New User Created!")
                        self.dismiss(animated: true, completion: nil)
                    }
            })
        }
    }
    
    func dismissAndShowError(_ error: Error) {
        SVProgressHUD.dismiss()
        SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
