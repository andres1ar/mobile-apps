//
//  MainVC.swift
//  Open Thoughts
//
//  Created by Andres Alderete on 2019-09-05.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import GoogleSignIn
import FBSDKLoginKit
import TwitterKit

enum ThoughtCategory: String {
    case serious = "serious"
    case funny = "funny"
    case crazy = "crazy"
    case popular = "popular"
}

class MainVC: UIViewController, ThoughtDelegate {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableview: UITableView!
    
    private var thoughts = [Thought]()
    private var thoughtsCollectionRef: CollectionReference!
    private var thoughtsListener: ListenerRegistration!
    private var selectedCategory = ThoughtCategory.funny.rawValue
    
    //handle for authentication
    private var handle: AuthStateDidChangeListenerHandle?
    
    //Login manager
    let loginManager = LoginManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.estimatedRowHeight = 80
        tableview.rowHeight = UITableView.automaticDimension
        
        thoughtsCollectionRef = Firestore.firestore().collection(THOUGHTS_REF)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            if user == nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC = storyboard.instantiateViewController(withIdentifier: "loginVC")
                self.present(loginVC, animated: true, completion: nil)
            } else {
                self.setListener()
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if thoughtsListener != nil {
            thoughtsListener.remove()
        }
    }
    
    func thoughtOptionsTapped(thought: Thought) {
        //TODO: Add edit thought
        //This is where we create the alert to handle the deletion.
        let alert = UIAlertController(title: "Delete", message: "Do you want to delete your thought?", preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete Thought", style: .default) { (action) in
            //delete thought
            
            self.delete(collection: Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId)
                .collection(COMMENTS_REF), completion: { (error) in
                    if let error = error {
                        debugPrint("Couldnt delete subcollection: \(error.localizedDescription)")
                    } else {
                        Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId)
                            .delete(completion: { (error) in
                                if let error = error {
                                    debugPrint("Couldnt delete thought: \(error.localizedDescription)")
                                } else {
                                    alert.dismiss(animated: true, completion: nil)
                                }
                            })
                    }
            })
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func delete(collection: CollectionReference, batchSize: Int = 100, completion: @escaping (Error?) -> ()) {
        
        //Limit query to avoid out-of-memory errors on large collections.
        //When deleting a collection guaranteed to fit in memory, batching can be avoided entirely.
        
        collection.limit(to: batchSize).getDocuments { (docset, error) in
            // An error occured.
            guard let docset = docset else {
                completion(error)
                return
            }
            //There's nothing to delete.
            guard docset.count > 0 else {
                completion(nil)
                return
            }
            
            let batch = collection.firestore.batch()
            docset.documents.forEach { batch.deleteDocument($0.reference) }
            
            batch.commit { (batchError) in
                if let batchError = batchError {
                    // Stop the deletion process and handle the error. Some elements
                    // may have been deleted.
                    completion(batchError)
                } else {
                    self.delete(collection: collection, batchSize: batchSize, completion: completion)
                }
            }
        }
    }
    
    func setListener() {
        
        if selectedCategory == ThoughtCategory.popular.rawValue {
            thoughtsListener = thoughtsCollectionRef
                .order(by: NUM_LIKES, descending: true)
                .addSnapshotListener { (snapshot, error) in
                    if let error = error {
                        debugPrint("Error fetching documents: \(error)")
                    } else {
                        self.thoughts.removeAll()
                        self.thoughts = Thought.paseData(snapshot: snapshot)
                        self.tableview.reloadData()
                    }
            }
        } else {
            thoughtsListener = thoughtsCollectionRef
                .whereField(CATEGORY, isEqualTo: selectedCategory)
                .order(by: TIMESTAMP, descending: true)
                .addSnapshotListener { (snapshot, error) in
                    if let error = error {
                        debugPrint("Error fetching documents: \(error)")
                    } else {
                        self.thoughts.removeAll()
                        self.thoughts = Thought.paseData(snapshot: snapshot)
                        self.tableview.reloadData()
                    }
            }
        }
    }
    
    @IBAction func categoryChanged(_ sender: Any) {
        
        switch segmentControl.selectedSegmentIndex {
        case 0:
            selectedCategory = ThoughtCategory.funny.rawValue
        case 1:
            selectedCategory = ThoughtCategory.serious.rawValue
        case 2:
            selectedCategory = ThoughtCategory.crazy.rawValue
        default:
            selectedCategory = ThoughtCategory.popular.rawValue
        }
        thoughtsListener.remove()
        setListener()
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        let logOutAlert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .actionSheet)
        let logoutAction = UIAlertAction(title: "Logout", style: .default) { (action) in
            let firebaseAuth = Auth.auth()
            do {
                self.logoutSocial()
                try firebaseAuth.signOut()
            } catch let signoutError as NSError {
                debugPrint("Error signing out: \(signoutError)")
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        logOutAlert.addAction(logoutAction)
        logOutAlert.addAction(cancelAction)
        
        present(logOutAlert, animated: true, completion: nil)
    }
    
    
    func logoutSocial() {
        guard let user = Auth.auth().currentUser else { return }
        for info in (user.providerData) {
            switch info.providerID {
            case GoogleAuthProviderID:
                GIDSignIn.sharedInstance()?.signOut()
                print("Google")
            case TwitterAuthProviderID:
                print("Twitter")
                TWTRTwitter.sharedInstance().sessionStore.logOutUserID(Auth.auth().currentUser!.uid)
            case FacebookAuthProviderID:
                print("Facebook")
                loginManager.logOut()
            default:
                break
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toComments" {
            if let destinationVC = segue.destination as? CommentsVC {
                if let thought = sender as? Thought {
                    destinationVC.thought = thought
                }
            }
        }
    }
}

extension MainVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return thoughts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "thoughtCell", for: indexPath) as? ThoughtCell {
            cell.configureCell(withThought: thoughts[indexPath.row], delegate: self)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toComments", sender: thoughts[indexPath.row])
    }
}
