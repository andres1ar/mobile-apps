//
//  AppDelegate.swift
//  Open Thoughts
//
//  Created by Andres Alderete on 2019-09-05.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Dynamic shortcut
        let createThoughtShortcut = UIMutableApplicationShortcutItem(type: "\(String(describing: Bundle.main.bundleIdentifier)).createThought", localizedTitle: "Create Thought", localizedSubtitle: nil, icon: UIApplicationShortcutIcon.init(templateImageName: "addThoughtIcon"), userInfo: nil)
        
        application.shortcutItems = [createThoughtShortcut]
        
        //Firebase
        FirebaseApp.configure()
        
        //Google
        GIDSignIn.sharedInstance()?.clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance()?.delegate = self
        
        //Facebook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //Twitter
        let key = Bundle.main.object(forInfoDictionaryKey: "consumerKey")
        let secret = Bundle.main.object(forInfoDictionaryKey: "consumerSecret")
        if let key = key as? String, let secret = secret as? String {
            TWTRTwitter.sharedInstance().start(withConsumerKey: key, consumerSecret: secret)
        }
        
        return true
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            debugPrint("Could not login with Google: \(error)")
        } else {
            print("Logged in with Google")
            guard let controller = GIDSignIn.sharedInstance()?.uiDelegate as? LoginVC else { return }
            guard let authentication = user.authentication else { return }
            let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
            controller.firebaseLogin(credential)
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let returnGoogle = GIDSignIn.sharedInstance().handle(url,
                                                             sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                             annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        let returnFB = ApplicationDelegate.shared.application(app, open: url, options: options)
        
        let returnTwitter = TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        
        return returnGoogle || returnFB || returnTwitter
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        
        let navVC = window?.rootViewController as! UINavigationController
        
        //Storyboard identifiers
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyboard.instantiateViewController(withIdentifier: "mainVC") as! MainVC
        let addThoughtVC = storyboard.instantiateViewController(withIdentifier: "addThoughtVC") as! AddThoughtVC
        
        //Check if the user is authenticated before allowing to create a thought
        if Auth.auth().currentUser == nil {
            //not authenticated, sending to the mainVC to trigger the login screen
            navVC.pushViewController(mainVC, animated: true)
             completionHandler(false)
        } else {
            //Authenticated, pushing the corresponding vc
            if let type = shortcutItem.type.components(separatedBy: ".").last {
                if type == "createThought" {
                    navVC.pushViewController(addThoughtVC, animated: true)
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }
        }
    }
}

