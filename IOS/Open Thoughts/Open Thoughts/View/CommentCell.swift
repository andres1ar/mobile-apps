//
//  CommentCell.swift
//  Open Thoughts
//
//  Created by Andres Alderete on 2019-09-07.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import FirebaseAuth

protocol CommentDelegate {
    func commentOptionsTapped(comment: Comment)
}

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var timestampLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var optionsMenu: UIImageView!
    
    
    private var comment: Comment!
    private var delegate: CommentDelegate?
    
    func configureCell(withComment comment: Comment, delegate: CommentDelegate?) {
        usernameLbl.text = comment.username
        commentLbl.text = comment.commentTxt
        optionsMenu.isHidden = true
        optionsMenu.isUserInteractionEnabled = true
        self.comment = comment
        self.delegate = delegate
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, hh:mm"
        let timestamp = formatter.string(from: comment.timestamp)
        timestampLbl.text = timestamp
        
        if comment.userId == Auth.auth().currentUser?.uid {
            optionsMenu.isHidden = false
            let tap = UITapGestureRecognizer(target: self, action: #selector(commentOptionsTapped))
            optionsMenu.addGestureRecognizer(tap)
        }
    }
    
    @objc func commentOptionsTapped() {
        delegate?.commentOptionsTapped(comment: comment)
    }
}
