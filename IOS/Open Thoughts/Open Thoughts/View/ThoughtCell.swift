//
//  ThoughtCell.swift
//  Open Thoughts
//
//  Created by Andres Alderete on 2019-09-05.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Firebase

protocol ThoughtDelegate {
    func thoughtOptionsTapped(thought: Thought)
}

class ThoughtCell: UITableViewCell {
    
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var timestampLbl: UILabel!
    @IBOutlet weak var thoughtTextLbl: UILabel!
    @IBOutlet weak var likesImage: UIImageView!
    @IBOutlet weak var likesNumLbl: UILabel!
    @IBOutlet weak var commentsNumLbl: UILabel!
    @IBOutlet weak var optionsMenu: UIImageView!
    
    private var thought: Thought!
    private var delegate: ThoughtDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Gesture recognizer for like button
        let tap = UITapGestureRecognizer(target: self, action: #selector(likeTapped))
        likesImage.addGestureRecognizer(tap)
        likesImage.isUserInteractionEnabled = true
    }
    
    @objc func likeTapped() {
        
        let currentUserID = Auth.auth().currentUser!.uid
        Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId).collection(LIKED_BY_REF).getDocuments { (documents, error) in
            
            guard let documentsResult = documents?.documents else { return }
            
            if documentsResult.count > 0 {
                for document in documentsResult {
                    if document.documentID == currentUserID {
                        //remove like
                        self.removeLike(forUser: currentUserID)
                        return
                    } else {
                        //If the document doesn't exists, create the document and add one like
                        self.addLike(forUser: currentUserID)
                    }
                }
            } else {
                //add a new
                self.addLike(forUser: currentUserID)
            }
        }
    }
    
    func addLike(forUser userID: String) {
        Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId).collection(LIKED_BY_REF).document(userID).setData([userID : true])
        
        Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId)
            .updateData([NUM_LIKES : thought.numLikes + 1])
    }
    
    func removeLike(forUser userID: String) {
        Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId).collection(LIKED_BY_REF).document(userID).delete { (error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            } else {
                //remove a like
                Firestore.firestore().collection(THOUGHTS_REF).document(self.thought.documentId)
                    .updateData([NUM_LIKES : self.thought.numLikes - 1])
            }
        }
    }
    
    func configureCell(withThought thought: Thought, delegate: ThoughtDelegate) {
        optionsMenu.isHidden = true
        self.thought = thought
        self.delegate = delegate
        usernameLbl.text = thought.username
        thoughtTextLbl.text = thought.thoughtTxt
        likesNumLbl.text = String(thought.numLikes)
        commentsNumLbl.text = String(thought.numComments)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, hh:mm"
        let timestamp = formatter.string(from: thought.timestamp)
        timestampLbl.text = timestamp
        
        //Check if the thought belongs to the user before adding a menu button
        if thought.userId == Auth.auth().currentUser?.uid {
            optionsMenu.isHidden = false
            optionsMenu.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(thoughtOptionsTapped))
            optionsMenu.addGestureRecognizer(tap)
        }
    }
    
    @objc func thoughtOptionsTapped() {
        delegate?.thoughtOptionsTapped(thought: thought)
    }
}
