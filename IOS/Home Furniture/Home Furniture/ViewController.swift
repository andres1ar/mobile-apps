//
//  ViewController.swift
//  Home Furniture
//
//  Created by Andres Alderete on 2019-02-08.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let furnitures =  [
        [
            Furniture(name:"Couch",description:"A comfy place to sit down"),Furniture(name:"Television",description:"A device to watch your favorite shows"),Furniture(name:"Coffee Table",description:"A table to have a coffee")
        ],
        [
            Furniture(name:"Stove",description:"An appliance to cook stuff"),Furniture(name:"Oven",description:"An appliance to make great pizza"),Furniture(name:"Sink",description:"The place where you should wash your dishes")
        ],
        [Furniture(name:"Bed",description:"A nice place to relax and sleep"),Furniture(name:"Desk",description:"Necessary item to study "),Furniture(name:"Closet",description:"You can put your clothes here")]
        
    ]
    
    
    override func viewDidLoad() {
        
        // DataSource and Delegate
        tableView.dataSource = self
        tableView.delegate = self
        
        // Automatic cell height
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = tableView.rowHeight
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addPicture"{
            let indexPath = tableView.indexPathForSelectedRow!
            let furniture = furnitures[indexPath.section][indexPath.row]
            
            let addpictureTableViewController = segue.destination as! FurnitureViewController
            
            addpictureTableViewController.furniture = furniture
        }
    }
}

extension ViewController: UITableViewDataSource {
    
    // Optional Section Header Data Source -----------
    func numberOfSections(in tableView: UITableView) -> Int {
        return furnitures.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let myHeaders = ["LIVING ROOM", "KITCHEN", "BEDROOM"]
        return myHeaders[section]
    }
    
    
    // Mandatory DataSource protol methods -----------
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return furnitures[section].count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = furnitures[indexPath.section][indexPath.row].name
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    
    // Create Section View ..........................
    func tableView(_ tableView: UITableView,
                   viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        
        headerView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        let headerLabel = UILabel(frame: CGRect(x: 0,
                                                y: 0,
                                                width: tableView.bounds.size.width,
                                                height: tableView.bounds.size.height))
        
        headerLabel.font = UIFont(name: "Verdana", size: 16)
        headerLabel.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    // Section Header Height
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
}
