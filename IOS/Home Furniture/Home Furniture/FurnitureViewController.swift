//
//  FurnitureViewController.swift
//  Home Furniture
//
//  Created by Andres Alderete on 2019-02-08.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class FurnitureViewController: UIViewController, UINavigationControllerDelegate{
    
    var furniture: Furniture?
    var image: UIImage?
    
    @IBOutlet weak var imgButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let furniture = furniture {
            titleLabel.text = furniture.name
            descriptionLabel.text = furniture.description
        }
    }
    
    @IBAction func imageButton(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let alertController = UIAlertController(title: "Choose Image Source",
                                                message: nil,
                                                preferredStyle: .actionSheet)
        
        //camera
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {action in
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
            })
            alertController.addAction(cameraAction)
        }
        
        //photo library
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let photoLibraryAction = UIAlertAction(title: "Photo Library", style: .default, handler: {action in
                imagePicker.sourceType = .photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
            })
            alertController.addAction(photoLibraryAction)
        }
        
        //cancel
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        //set trigger and present
        alertController.popoverPresentationController?.sourceView = sender
        present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func shareButtonPressed(_ sender: UIBarButtonItem) {
        guard image != nil else {return}
        
        let activityController = UIActivityViewController(activityItems: [image!], applicationActivities: nil)
print("after let")
        activityController.popoverPresentationController?.barButtonItem = sender
print("after activityController")
        present(activityController, animated: true, completion: nil)
        print("after present")
    }
}

extension FurnitureViewController: UIImagePickerControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedImage = info[.originalImage] as? UIImage {
            imgButton.setBackgroundImage(selectedImage, for: .normal)
            image = selectedImage
            imgButton.setTitle("", for: .normal)
            dismiss(animated: true, completion: nil)
        }
    }
}
