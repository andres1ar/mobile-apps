//
//  Furniture.swift
//  Home Furniture
//
//  Created by Andres Alderete on 2019-02-08.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation

class Furniture {
    var name: String
    var description: String
    
    
    init(name: String, description: String){
        self.name = name
        self.description = description
    }
}
