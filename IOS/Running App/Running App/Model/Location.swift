//
//  Location.swift
//  Running App
//
//  Created by Andres Alderete on 2019-08-28.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Location: Object {
    dynamic public private(set) var latitude = 0.0
    dynamic public private(set) var longitude = 0.0
    
    convenience init(latitude: Double, longitude: Double) {
        self.init()
        self.latitude = latitude
        self.longitude = longitude
    }
}
