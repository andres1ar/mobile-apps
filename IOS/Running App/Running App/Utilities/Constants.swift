//
//  Constants.swift
//  Running App
//
//  Created by Andres Alderete on 2019-08-27.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation

let REALM_QUEUE = DispatchQueue(label: "realmQueue")
let REALM_RUN_CONFIG = "realmRunConfig"

