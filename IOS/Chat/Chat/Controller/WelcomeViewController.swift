//
//  WelcomeViewController.swift
//  Chat
//
//  Created by Andres Alderete on 2019-04-24.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Firebase

class WelcomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //If there is a logged in user, by pass this screen and go straight to ChatViewController
        
        if Auth.auth().currentUser != nil {
            performSegue(withIdentifier: "goToChat", sender: self)
        }
    }
}
