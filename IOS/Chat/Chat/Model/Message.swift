//
//  Message.swift
//  Chat
//
//  Created by Andres Alderete on 2019-04-24.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation

class Message {
    
    var sender : String = ""
    var messageBody : String = ""
}
