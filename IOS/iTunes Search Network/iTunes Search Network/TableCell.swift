//
//  TableCell.swift
//  iTunes Search Network
//
//  Created by Andres Alderete on 2019-02-13.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    func update(with storeItem: StoreItem) -> Void {
        
        imgView.layer.cornerRadius = 8.0
        imgView.clipsToBounds = true
        
        guard let url = storeItem.artworkURL.withHTTPS() else { return }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data,
                let image = UIImage(data: data) {
                DispatchQueue.main.sync {
                    self.titleLabel.text = storeItem.name
                    self.authorLabel.text = storeItem.artist
                    self.imgView.image = image
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
        }
        task.resume()
    }
}
