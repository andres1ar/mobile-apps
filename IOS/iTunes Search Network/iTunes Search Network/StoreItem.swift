//
//  StoreItem.swift
//  iTunes Search Network
//
//  Created by Andres Alderete on 2019-02-14.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation

struct StoreItems: Codable {
    let results: [StoreItem]
}

struct StoreItem: Codable {
    var name: String
    var artist: String
    var description: String
    var kind: String
    var artworkURL: URL
    
    enum CodingKeys: String, CodingKey {
        // Mapping between enum CodingKeys with JSON keys or accessing JSON keys
        case name = "trackName"
        case artist = "artistName"
        case kind = "kind"
        case description = "description"
        case artworkURL = "artworkUrl100"
    }
    
    enum AdditionalKeys: String, CodingKey {
        case longDescription
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        // Mapping betwen instance variables and CodingKeys cases
        name = try values.decode(String.self, forKey: CodingKeys.name)
        artist = try values.decode(String.self, forKey: CodingKeys.artist)
        kind = try values.decode(String.self, forKey: CodingKeys.kind)
        artworkURL = try values.decode(URL.self, forKey: CodingKeys.artworkURL)
        
        if let description = try? values.decode(String.self, forKey: CodingKeys.description) {
            // 1- If managed to extract some information from JSON by "description" key assign it to "description" instance variable as usual
            self.description = description
        } else {
            // 2- Otherwise consider an other key which is introduced in other enum "AdditionalKeys" for decoding
            let additionalValues = try decoder.container(keyedBy: AdditionalKeys.self)
            
            // 3- Map enum case "longDescription" which has the same key in JSON responce with instance variable "description"
            description = (try? additionalValues.decode(String.self, forKey: AdditionalKeys.longDescription)) ?? ""
        }
    }
    
    static let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    static let archiveURL = documentDirectory.appendingPathComponent("itens").appendingPathExtension("me")
    
}
