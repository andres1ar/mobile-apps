//
//  FirstViewController.swift
//  iTunes Search Network
//
//  Created by Andres Alderete on 2019-02-13.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let storeItemController = StoreItemController()
    
    var items = [StoreItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Table delegates
        table.delegate = self
        table.dataSource = self
        
        //Search Bar delegate
        searchBar.delegate = self
        
        updateUI(with: items)
        
    }

    func updateUI(with storeItems: [StoreItem]){
        items = storeItems
        DispatchQueue.main.async {
            self.table.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? TableCell else {
            return UITableViewCell()
        }
       let storeItem = items[indexPath.row]
        
      cell.update(with: storeItem)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func searchQuery(searchText: String, media: String){
         let searchQuery = [
            "term": searchText,
            "media": media,
            "lang": "en_us",
            "limit": "30"
        ]
        
        storeItemController.fetchItems(matching: searchQuery) { (items) in
            print(items!)
            self.items = items!
            DispatchQueue.main.async {
            self.table.reloadData()
            }
            
        }
    }
    
    //Search Bar
    //Search after pressing "Search" button
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        switch searchBar.selectedScopeButtonIndex {
        case 0:
            items.removeAll()
            searchQuery(searchText: searchBar.text!, media: "movie")
        case 1:
            items.removeAll()
            searchQuery(searchText: searchBar.text!, media: "music")
        case 2:
            items.removeAll()
            searchQuery(searchText: searchBar.text!, media: "software")
        case 3:
            items.removeAll()
            searchQuery(searchText: searchBar.text!, media: "ebook")
        default:
            break
        }
        searchBar.resignFirstResponder()
        table.reloadData()
    }
    
    //Search after pressing any button
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//            switch searchBar.selectedScopeButtonIndex{
//            case 0:
//                searchQuery(searchText: searchText, media: "movie")
//                return
//            case 1:
//                searchQuery(searchText: searchText, media: "music")
//                return
//            case 2:
//                searchQuery(searchText: searchText, media: "software")
//                return
//            case 3:
//                searchQuery(searchText: searchText, media: "ebook")
//                return
//            default:
//                return
//            }
//    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        switch selectedScope {
        case 0:
            items.removeAll()
            searchQuery(searchText: searchBar.text ?? "", media: "movie")
        case 1:
            items.removeAll()
            searchQuery(searchText: searchBar.text ?? "", media: "music")
        case 2:
        items.removeAll()
        searchQuery(searchText: searchBar.text ?? "", media: "software")
        case 3:
            items.removeAll()
            searchQuery(searchText: searchBar.text ?? "", media: "ebook")
        default:
            break
        }
        table.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewDetail"{
            let indexPath = table.indexPathForSelectedRow!
            let item = items[indexPath.row]
            let detailTableVC = segue.destination as! DetailTableViewController
            detailTableVC.item = item
        }
    }
}
