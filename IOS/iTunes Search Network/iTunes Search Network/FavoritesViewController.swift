//
//  SecondViewController.swift
//  iTunes Search Network
//
//  Created by Andres Alderete on 2019-02-13.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var favTable: UITableView!
    var items = [StoreItem]()
    var allItems = [StoreItem]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // DataSource and Delegate
        favTable.dataSource = self
        favTable.delegate = self

        //Load from file
        items = loadFromFile()
        
        //Create a copy of items to use in filter
        allItems = loadFromFile()
    }
    
    func loadFromFile() -> [StoreItem] {
    let propertyListDecoder = PropertyListDecoder()
    if let retrievedItemsData = try? Data(contentsOf: StoreItem.archiveURL), let decodedItems = try? propertyListDecoder.decode(Array<StoreItem>.self, from: retrievedItemsData){
    return decodedItems
    }
    return items
    }
   
    @IBAction func editButtonPressed(_ sender: UIBarButtonItem) {
        let tableViewEditingMode = favTable.isEditing
        favTable.setEditing(!tableViewEditingMode, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete //.insert or .none
    }
    
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            items.remove(at: indexPath.row)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .automatic)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    // Override to support rearranging the table view.
    func tableView(_ tableView: UITableView,
                            moveRowAt fromIndexPath: IndexPath,
                            to: IndexPath) {
        
        let movedItem = items.remove(at: fromIndexPath.row)
        items.insert(movedItem, at: to.row)
    }
    
    @IBAction func saveList(_ sender: UIBarButtonItem) {
        let propertyListEncoder = PropertyListEncoder()
        let encodedItemsArray = try? propertyListEncoder.encode(items)
        try? encodedItemsArray?.write(to: StoreItem.archiveURL, options: .noFileProtection)
    }
    
    
    @IBAction func sortButtonPressed(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            items = allItems
            favTable.reloadData()
        case 1:
            items = allItems.filter {$0.kind == "feature-movie"}
            favTable.reloadData()
        case 2:
            items = allItems.filter {$0.kind == "song"}
            favTable.reloadData()
            return
        case 3:
            items = allItems.filter {$0.kind == "software"}
            favTable.reloadData()
            return
        case 4:
            items = allItems.filter {$0.kind == "ebook"}
            favTable.reloadData()
            return
        default:
            return
        }
    }
    
    @IBAction func shareButtonPressed(_ sender: UIBarButtonItem) {
        
        var list = ""
        
        items.forEach { (item) in
            list = list +
            """
            \(item.name)
            \(item.artist)
            \(item.description)
            \(item.kind)

            """
        }
        print(list)
        
        let activityController = UIActivityViewController(activityItems: [list], applicationActivities: nil)
        activityController.popoverPresentationController?.barButtonItem = sender
        present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func unwindToFavoritesViewController(segue: UIStoryboardSegue){
        
        guard segue.identifier == "fromDetails" else {return}
        let sourceViewController = segue.source as! DetailTableViewController

        if let item = sourceViewController.item {
            items.append(item)
           favTable.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewFav"{
            let indexPath = favTable.indexPathForSelectedRow!
            let item = items[indexPath.row]
            let detailTableVC = segue.destination as! DetailTableViewController
            detailTableVC.item = item
        }
    }
}

extension FavoritesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? TableCell else {
            return UITableViewCell()
        }
        let storeItem = items[indexPath.row]
        cell.update(with: storeItem)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

extension FavoritesViewController: UITableViewDelegate {
    
}



