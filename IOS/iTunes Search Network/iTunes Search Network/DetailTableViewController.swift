//
//  DetailTableViewController.swift
//  iTunes Search Network
//
//  Created by Andres Alderete on 2019-02-13.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {
    
    @IBOutlet weak var imgView: UIImageView!
  
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    @IBOutlet weak var saveToFavoritesButton: UIBarButtonItem!
    
    var item: StoreItem?
    var items = [StoreItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let item = item {
            nameLabel.text = item.name
            authorLabel.text = item.artist
            let data = try? Data(contentsOf: item.artworkURL)
            imgView.image = UIImage(data: data!)
            descriptionLabel.text = item.description
        }
    }
    
    @IBAction func saveToFavPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "saveFavorite", sender: nil)
    }
}
