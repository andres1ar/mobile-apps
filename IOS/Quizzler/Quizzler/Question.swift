//
//  Question.swift
//  Quizzler
//
//  Created by Andres Alderete on 2019-04-18.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation

class Question {
    
    let questionText : String
    let answer : Bool
    
    init(text: String, correctAnswer: Bool) {
        questionText = text
        answer = correctAnswer
    }
}
