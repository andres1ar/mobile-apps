//
//  Message.swift
//  Breakpoint
//
//  Created by Andres Alderete on 2019-08-29.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation

class Message {
    private var _content: String
    private var _senderId: String
    
    var content: String {
        return _content
    }
    
    var senderId: String {
        return _senderId
    }
    
    init(content: String, senderId: String) {
        self._content = content
        self._senderId = senderId
    }
}
