//
//  AvatarCell.swift
//  Breakpoint
//
//  Created by Andres Alderete on 2019-09-01.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class AvatarCell: UICollectionViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    func configureCell(index: Int) {
        avatarImageView.image = UIImage(named: "icon\(index)" )
    }
}
