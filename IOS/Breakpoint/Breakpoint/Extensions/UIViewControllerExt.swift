//
//  UIViewControllerExt.swift
//  Breakpoint
//
//  Created by Andres Alderete on 2019-08-30.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

extension UIViewController {
    
    //Function to present a view controller animated from the right
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition, forKey:  kCATransition)
        
        present(viewControllerToPresent, animated: false, completion: nil)
    }
    
    //Function to dismiss
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window?.layer.add(transition, forKey:  kCATransition)
        
        dismiss(animated: false, completion: nil)
    }
}
