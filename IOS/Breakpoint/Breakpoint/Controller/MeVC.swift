//
//  MeVC.swift
//  Breakpoint
//
//  Created by Andres Alderete on 2019-08-29.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn

class MeVC: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    //TODO: Give the option to select a profile picture based on programming languages. Add a label to have a profile description. Implement login with facebook and google
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.emailLbl.text = Auth.auth().currentUser?.email
        if Auth.auth().currentUser != nil {
        getProfileIcon()
        }
    }
    
    func getProfileIcon() {
        DataService.instance.getProfileIcon(forUID: Auth.auth().currentUser!.uid) { (icon) in
            self.profileImage.image = UIImage(named: icon)
        }
    }

    @IBAction func signOutBtnPressed(_ sender: Any) {
        let logoutPopup = UIAlertController(title: "Logout?", message: "Are you sure you want to logout?", preferredStyle: .actionSheet)
        let logoutAction = UIAlertAction(title: "Logout?", style: .destructive) { (buttonTapped) in
            do {
            try Auth.auth().signOut()
                if GIDSignIn.sharedInstance()?.currentUser != nil {
                    GIDSignIn.sharedInstance()?.signOut()
                }
            let authVC = self.storyboard?.instantiateViewController(withIdentifier: "AuthVC") as? AuthVC
                self.present(authVC!, animated: true, completion: nil)
            } catch {
                print(error)
            }
        }
        logoutPopup.addAction(logoutAction)
        present(logoutPopup, animated: true, completion: nil)
    }
    
}
