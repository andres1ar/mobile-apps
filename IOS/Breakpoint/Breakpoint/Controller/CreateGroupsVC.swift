//
//  CreateGroupsVC.swift
//  Breakpoint
//
//  Created by Andres Alderete on 2019-08-30.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import FirebaseAuth

class CreateGroupsVC: UIViewController {
    
    @IBOutlet weak var titleTextField: InsetTextField!
    @IBOutlet weak var descriptionTextField: InsetTextField!
    @IBOutlet weak var emailSearchTextField: InsetTextField!
    @IBOutlet weak var groupMembersLbl: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    //Array to hold the searched users
    var emailArray = [String]()
    //Array to hold the selected users
    var chosenUserArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        emailSearchTextField.delegate = self
        emailSearchTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        doneBtn.isHidden = true
    }
    
    @objc func textFieldDidChange() {
        if emailSearchTextField.text == "" {
            emailArray = []
            tableView.reloadData()
        } else {
            DataService.instance.getEmail(forSearchQuery: emailSearchTextField.text!) { (returnedEmailArray) in
                self.emailArray = returnedEmailArray
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        if titleTextField.text != "" && descriptionTextField.text != "" {
            DataService.instance.getIDs(forUsernames: chosenUserArray) { (idsArray) in
                var userIds = idsArray
                //Add yourself on the first position
                userIds.insert(Auth.auth().currentUser!.uid, at: 0)
//                userIds.append(Auth.auth().currentUser!.uid)
                
                DataService.instance.createGroup(withTitle: self.titleTextField.text!, andDescription: self.descriptionTextField.text!, forUserIds: userIds, handler: { (groupCreated) in
                    if groupCreated {
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        print("Group could not be created.")
                    }
                })
            }
        }
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension CreateGroupsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emailArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "userCell") as? UserCell else { return UITableViewCell() }
        //TODO: Get the real icon for each user
        let profileImage = UIImage(named: "defaultProfileImage")
        
        //Check if the user was selected before when modifying the search
        let selectedStatus = chosenUserArray.contains(emailArray[indexPath.row])
        
        cell.configureCell(profileImage: profileImage!, email: emailArray[indexPath.row], isSelected: selectedStatus)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? UserCell else { return }
        if !chosenUserArray.contains(cell.emailLbl.text!) {
            chosenUserArray.append(cell.emailLbl.text!)
            //Add selected user to the label using a comma to separate them nicely
            groupMembersLbl.text = chosenUserArray.joined(separator: ", ")
            doneBtn.isHidden = false
        } else {
            //Filter the user to remove it from the array
            chosenUserArray = chosenUserArray.filter({ $0 != cell.emailLbl.text})
            //If the array contains less that 1 selected user, the label will display back the default text
            if chosenUserArray.count >= 1 {
                groupMembersLbl.text = chosenUserArray.joined(separator: ", ")
            } else {
                groupMembersLbl.text = "add people to your group"
                doneBtn.isHidden = true
            }
            
        }
        //Deselect to remove the highlight gray color
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CreateGroupsVC: UITextFieldDelegate {
    //Dismiss keyboard after pressing return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
