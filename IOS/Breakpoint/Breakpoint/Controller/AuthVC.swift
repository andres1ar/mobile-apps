//
//  AuthVC.swift
//  Breakpoint
//
//  Created by Andres Alderete on 2019-08-29.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import FirebaseAuth
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookLogin
import FacebookCore
import GoogleSignIn

class AuthVC: UIViewController, GIDSignInUIDelegate {
    
    @IBOutlet weak var googleSignInButton: GIDSignInButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance()?.uiDelegate = self
        if Auth.auth().currentUser != nil {
//        GIDSignIn.sharedInstance().signIn()
//            dismiss(animated: true, completion: nil)
        }
        
        googleSignInButton.style = .iconOnly
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Auth.auth().currentUser != nil {
            dismiss(animated: true, completion: nil)
        }
        if GIDSignIn.sharedInstance()?.currentUser != nil {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func facebookSignInBtnPressed(_ sender: Any) {
        if AccessToken.current != nil {
            dismiss(animated: true, completion: nil)
            print("already logged in")
        } else {
            let manager = LoginManager()
            
            manager.logIn(permissions: [.email], viewController: self) { (loginResult) in
                
                switch loginResult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    print(grantedPermissions)
                    print(declinedPermissions)
                    
                    
                    let credential = FacebookAuthProvider.credential(withAccessToken: (accessToken.tokenString))
                    Auth.auth().signIn(with: credential) {( user, error) in
                        if let error = error {
                            print(error)
                            return
                        }
                        let currentUser = Auth.auth().currentUser
                        print(currentUser!)
                        
                        //Register user on the database
                        let userData = ["provider": currentUser?.providerID,
                                        "email": currentUser?.email,
                                        "iconName": "defaultProfileImage"]
                        DataService.instance.createDBUser(uid: currentUser!.uid, userData: userData as Dictionary<String, Any>)
                        
                        // Navigates back
                        self.dismiss(animated: true, completion: nil);
                    }
                }
            }
        }
    }
        
        @IBAction func googleSignInBtnPressed(_ sender: Any) {
        }
        
        
        @IBAction func signInWithEmailBtnPressed(_ sender: Any) {
            let loginVC = (storyboard?.instantiateViewController(withIdentifier: "LoginVC"))!
            present(loginVC, animated: true, completion: nil)
        }
        
        
}
