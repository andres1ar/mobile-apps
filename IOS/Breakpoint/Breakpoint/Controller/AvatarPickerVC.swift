//
//  AvatarPickerVC.swift
//  Breakpoint
//
//  Created by Andres Alderete on 2019-09-01.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import FirebaseAuth

class AvatarPickerVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
 //MARK: - Collection methods
extension AvatarPickerVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "avatarCell", for: indexPath) as? AvatarCell {
            cell.configureCell(index: indexPath.item)
            return cell
        }
        return AvatarCell()
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        var numOfColumns : CGFloat = 3
//        if UIScreen.main.bounds.width > 320 {
//            numOfColumns = 4
//        }
//        let spaceBetweenCells: CGFloat = 10
//        let padding : CGFloat = 40
//        let cellDimension = ((collectionView.bounds.width - padding) - (numOfColumns - 1) * spaceBetweenCells) / numOfColumns
//        return CGSize(width: cellDimension, height: cellDimension)
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let iconName = "icon\(indexPath.item)"
        DataService.instance.updateProfileIcon(forUID: Auth.auth().currentUser!.uid, withIcon: iconName) { (complete) in
            if complete {
                print("Profile icon updated")
                self.dismiss(animated: true, completion: nil)
            } else {
                print("Error updating profile icon name to Firebase")
            }
        }
        
    }
    
}
