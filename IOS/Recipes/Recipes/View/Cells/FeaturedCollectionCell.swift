//
//  FeaturedCollectionCell.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-06-20.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class FeaturedCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var featureRecipeImage: UIImageView!
}
