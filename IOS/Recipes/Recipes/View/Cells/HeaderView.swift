//
//  HeaderView.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-06-20.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class HeaderView: UITableViewHeaderFooterView {

    
    @IBOutlet weak var headerLabel: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
