//
//  CustomLabel.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-03.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {

    //First loading function
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultSetup()
    }
    
    //First required to load
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defaultSetup()
    }
    
    //Sets up the default spacing to 1
    func defaultSetup(){
        //Label spaceing
        let labelSpace = 1
        let labelAttributedString = NSMutableAttributedString(string: text!)
        labelAttributedString.addAttribute(NSAttributedString.Key.kern, value: labelSpace, range: NSMakeRange(0, labelAttributedString.length))
        attributedText = labelAttributedString
    }
    
    //Sets the spacing of text
    func setSpacing(space: CGFloat){
        let labelAttributedString = NSMutableAttributedString(string: text!)
        labelAttributedString.addAttribute(NSAttributedString.Key.kern, value: space, range: NSMakeRange(0, labelAttributedString.length))
        attributedText = labelAttributedString
        
    }
    
}
