//
//  ShadowView.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-06-20.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class ShadowView: UIView {

   
    override func draw(_ rect: CGRect) {
       applyShadow()
    }
    
    //Settings for the shadow on the cells
    func applyShadow() {
        
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 0, height: 8)
        self.layer.shadowRadius = 4
        let radii = CGSize(width: 4, height: 4)
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: radii).cgPath
        self.layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}
