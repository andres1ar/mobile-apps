//
//  CustomButton.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-03.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    //First loading function
    override init(frame: CGRect) {
        super.init(frame:frame)
        defaultSetup()
    }
    
    //First required loading function
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defaultSetup()
    }
    
    //Customizes the button
    func defaultSetup(){
        layer.cornerRadius = layer.frame.height/2
        layer.masksToBounds = true
        
        //Spacing for letters
        let spacing = 1.5
        let buttonAttributedString = NSMutableAttributedString(string: (titleLabel?.text)!)
        buttonAttributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, buttonAttributedString.length) )
        self.setAttributedTitle(buttonAttributedString, for: .normal)
    }
    
    func makeCustomWhiteButton(){
        //Sign up button
        layer.borderWidth = 2
        backgroundColor = .white
        layer.borderColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
        layer.cornerRadius = layer.frame.height/2
        layer.masksToBounds = true
        
        //Spaceing for sign up button
        let signUpSpacing = 1.5
        let signUpSpacingButtonAttributedString = NSMutableAttributedString(string: (titleLabel?.text)!)
        signUpSpacingButtonAttributedString.addAttribute(NSAttributedString.Key.kern, value: signUpSpacing, range: NSMakeRange(0, signUpSpacingButtonAttributedString.length) )
        self.setAttributedTitle(signUpSpacingButtonAttributedString, for: .normal)
    }
    
}
