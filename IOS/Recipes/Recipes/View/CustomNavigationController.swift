//
//  CustomNavigationController.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-03.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {

    //First loading function
    override func viewDidLoad() {
        super.viewDidLoad()
        makeBarInvisible()
    }
    
//Makes the navigation bar invisible and clear
    func makeBarInvisible(){
        
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
    }

}
