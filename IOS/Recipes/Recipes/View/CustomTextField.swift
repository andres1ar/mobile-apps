//
//  CustomTextField.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-03.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    //First loading function
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultSetUp()
    }
    
    //First required function
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defaultSetUp()
    }

    //Sets up the textfield to custom
    func defaultSetUp(){
        
        //Textfield
        layer.borderWidth = 1
        layer.borderColor = UIColor(cgColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)).cgColor
        attributedPlaceholder = NSAttributedString(string: placeholder!, attributes:[NSAttributedString.Key.foregroundColor : UIColor(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))])
        layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0)
    }
    
}
