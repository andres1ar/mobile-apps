//
//  URL+Helpers.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-24.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation

extension URL {
    func withQueries(_ queries: [String:String]) -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.queryItems = queries.compactMap { URLQueryItem(name: $0.0, value: $0.1) }
        return components?.url
    }
    
    func withHTTPS() -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.scheme = "https"
        
        return components?.url
    }
}
