//
//  DetailTableViewController.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-13.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import SVProgressHUD

class DetailTableViewController: UITableViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var publisherLabel: UILabel!
    @IBOutlet weak var socialRankLabel: UILabel!
    @IBOutlet weak var publisherUrlLabel: UILabel!
        
    var recipe: Recipe?
    var sourceURL: URL?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("viewDidLoad")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear")

        loadData()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
    }
    
    func loadData() {
        SVProgressHUD.show()
        if let recipe = recipe {
            let data = try? Data(contentsOf: recipe.image_url)
            imgView.image = UIImage(data: data!)
            
            titlelabel.text = recipe.title
            publisherLabel.text = "By: \(recipe.publisher_url)"
            //sourceUrlLabel.text = recipe.f2f_url
            socialRankLabel.text = "Rank: \(recipe.social_rank.rounded())"
            publisherUrlLabel.text = " Publisher's web site:\n \(recipe.publisher_url)"
            //ingredientsLabel.text = recipe.ingredients.joined(separator: "\n")
            sourceURL = URL(string: recipe.source_url)
            
        }
        tableView.reloadData()
        SVProgressHUD.dismiss()
    }
    
    
    @IBAction func openLinkButtonPressed(_ sender: UIButton) {
        if let sourceURL = sourceURL {
            UIApplication.shared.open(sourceURL)
        }
    }
    
    
    @IBAction func saveToFavPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "saveFavorite", sender: nil)
    }
}
