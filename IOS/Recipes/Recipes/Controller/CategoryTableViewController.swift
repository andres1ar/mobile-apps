//
//  CategoryTableViewController.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-04.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class CategoryTableViewController: UITableViewController {

    
    //fake data
    let categories = ["APPETIZER", "BREAKFAST & BRUNCH", "DESSERT", "BEVERAGES", "MAIN DISH", "PASTA", "SALAD", "SOUP"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        
        cell.categoryLabel.text = categories[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }


}
