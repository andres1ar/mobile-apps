//
//  HomeTableViewController.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-04.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class HomeTableViewController: UITableViewController, UISearchBarDelegate {
    
    
    //fake data
    let sections = ["FEATURED RECIPES", "LATEST"]
    let items = [
        ["Pasta"],
        ["pasta", "pasta", "pasta", "pasta"]
    ]
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var latestsRecipeList = [Recipe]()
    
    var featuredList = [Recipe]()
    
    
    var recipe : Recipe?
    
    //    let url = URL(string: "https://food2fork.com/api/search?key=e14100ba0f71a4fbb188bf2c0024a8cf")!
    
    let baseUrl = "https://food2fork.com/api/search?key=e14100ba0f71a4fbb188bf2c0024a8cf"
    
    let latestRecipesUrl = "https://food2fork.com/api/search?key=e14100ba0f71a4fbb188bf2c0024a8cf&sort=t"
    
    let baseDetailUrl = "https://food2fork.com/api/get?key=e14100ba0f71a4fbb188bf2c0024a8cf"
    
    //    let f2fKey = "e14100ba0f71a4fbb188bf2c0024a8cf"
    
    //    var finalUrl = ""
    //    let yummlyAppID = 5c575ada
    //    let yummlyAppKey = 93164a4ef6f38352ae84e2ca406f205b
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        //Recipe cells
//        let recipeCell = UINib(nibName: "RecipeCell", bundle: nil)
//        tableView.register(recipeCell, forCellReuseIdentifier: "RecipeCell")
        
        //Featured cells
        let featureCell = UINib(nibName: "FeatureCell", bundle: nil)
        tableView.register(featureCell, forCellReuseIdentifier: "FeatureCell")
        
        //Header labels
        let headerView = UINib(nibName: "HeaderView", bundle: nil)
        tableView.register(headerView, forHeaderFooterViewReuseIdentifier: "HeaderView")
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tableViewTapped))
//        tableView.addGestureRecognizer(tapGesture)
//
        
        //TableView
        //        tableView.delegate = self
        //        tableView.dataSource = self
        
        //SearchBar
        searchBar.delegate = self
        
        //loads items to array
        
        print("viewDidLoad")
        
        getFeaturedRecipes()
        getLatestRecipes()
        
        
    }
    
//    @objc func tableViewTapped() {
//
//        performSegue(withIdentifier: "viewDetail", sender: AnyClass.self)
//
//
//    }
    
    
    func getLatestRecipes(){
        getLatestRecipesData(url: latestRecipesUrl)
    }
    
    func getFeaturedRecipes() {
        getFeaturedRecipesData(url: baseUrl)
    }
    
    //    //MARK: - Networking
    //    /***************************************************************/
    //
    
    //Method to get JSON with 30 trend recipes
    func getLatestRecipesData(url: String) {
        
        SVProgressHUD.show()
        
        Alamofire.request(url, method: .get)
            .responseJSON { response in
                if response.result.isSuccess {
                    
                    print("Sucess! Got the top recipes data")
                    let recipeJSON : JSON = JSON(response.result.value!)
                    
                    self.parseRecipesJSON(json: recipeJSON)
                    
                } else {
                    print("Error: \(String(describing: response.result.error))")
                    SVProgressHUD.dismiss()
                }
        }
    }
    
    //Method to get JSON with featured recipes
    func getFeaturedRecipesData(url: String) {
        
        SVProgressHUD.show()
        
        Alamofire.request(url, method: .get)
            .responseJSON { response in
                if response.result.isSuccess {
                    
                    print("Sucess! Got the featured recipes data")
                    let recipeJSON : JSON = JSON(response.result.value!)
                    
                    self.parseFeaturedRecipesJSON(json: recipeJSON)
                    
                } else {
                    print("Error: \(String(describing: response.result.error))")
                    SVProgressHUD.dismiss()
                }
        }
    }
    
    //method to get the JSON of a single detailed recipe (not used)
    func getRecipeDetailsData(url: String){
        Alamofire.request(url, method: .get).responseJSON { (response) in
            if response.result.isSuccess {
                print("Success! Got the recipe data")
                
                let recipeJSON : JSON  = JSON(response.result.value!)
                self.parseRecipe(with: recipeJSON)
                
            }else {
                print("Error \(response.result.error!)")
            }
            
        }
        print("getting data of recipe")
        
    }
    
    //Method to get JSON of searched recipe
    func getRecipesQueryData(searchText: String){
        
        let url = baseUrl + "&q=\(searchText)"
        
        SVProgressHUD.show()
        
        Alamofire.request(url, method: .get)
            .responseJSON { response in
                if response.result.isSuccess {
                    
                    print("Sucess! Got the search recipes data")
                    let recipeJSON : JSON = JSON(response.result.value!)
                    
                    self.parseRecipesJSON(json: recipeJSON)
                    
                } else {
                    print("Error: \(String(describing: response.result.error))")
                    SVProgressHUD.dismiss()
                }
        }
        
    }
    
    
    //    //MARK: - JSON Parsing
    //    /***************************************************************/
    
    //Method to parse and display data of recipes
    func parseRecipesJSON(json : JSON) {
        
        if let recipesResult = json["recipes"].array {
            
            for recipe in recipesResult {
                let f2f_url = recipe["f2f_url"].stringValue
                let image_url = recipe["image_url"].stringValue
                let recipe_id = recipe["recipe_id"].stringValue
                let publisher_url = recipe["publisher_url"].stringValue
                let publisher = recipe["publisher"].stringValue
                let title = recipe["title"].stringValue
                let social_rank = recipe["social_rank"].doubleValue
                let source_url = recipe["source_url"].stringValue
                
                let newRecipe = Recipe(publisher: publisher, f2f_url: f2f_url, title: title, source_url: source_url, recipe_id: recipe_id, image_url: URL(string: image_url)!, social_rank: social_rank, publisher_url: publisher_url)
                
                latestsRecipeList.append(newRecipe)
                
            }
            updateUI(with: latestsRecipeList)
            
            SVProgressHUD.dismiss()
        }
    }
    
    //Method to parse and display featured recipes
    func parseFeaturedRecipesJSON(json : JSON) {
        
        if let recipesResult = json["recipes"].array {
            
            for recipe in recipesResult {
                let f2f_url = recipe["f2f_url"].stringValue
                let image_url = recipe["image_url"].stringValue
                let recipe_id = recipe["recipe_id"].stringValue
                let publisher_url = recipe["publisher_url"].stringValue
                let publisher = recipe["publisher"].stringValue
                let title = recipe["title"].stringValue
                let social_rank = recipe["social_rank"].doubleValue
                let source_url = recipe["source_url"].stringValue
                
                let newRecipe = Recipe(publisher: publisher, f2f_url: f2f_url, title: title, source_url: source_url, recipe_id: recipe_id, image_url: URL(string: image_url)!, social_rank: social_rank, publisher_url: publisher_url)
                
                featuredList.append(newRecipe)
                
            }
            updateFeaturedUI(with: featuredList)
            
            SVProgressHUD.dismiss()
        }
    }
    
    //method to parse recipe with details as ingredients (not used)
    func parseRecipe(with json: JSON) {
        print("parsing recipe in json")
        
        if let recipeResult = json["recipe"]["publisher"].string {
            print("Parsing single recipe data")
            
            let publisher = recipeResult
            let f2f_url = json["recipe"]["f2f_url"].stringValue
            let ingredientsJson = json["recipe"]["ingredients"].arrayValue
            
            var ingredients = [String]()
            
            for ing in ingredientsJson {
                ingredients.append(ing.stringValue)
            }
            
            let source_url = json["recipe"]["source_url"].stringValue
            let recipe_id = json["recipe"]["recipe_id"].stringValue
            let image_url = json["recipe"]["image_url"].stringValue
            let social_rank = json["recipe"]["social_rank"].doubleValue
            let publisher_url = json["recipe"]["publisher_url"].stringValue
            let title = json["recipe"]["title"].stringValue
            
            recipe = Recipe(publisher: publisher, f2f_url: f2f_url, title: title, source_url: source_url, recipe_id: recipe_id, image_url: URL(string: image_url)!, social_rank: social_rank, publisher_url: publisher_url)
        }
    }
    
    func updateUI(with recipes: [Recipe]){
        latestsRecipeList = recipes
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func updateFeaturedUI(with recipes: [Recipe]){
        featuredList = recipes
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    //MARK: Tableview methods
    //
    /***************************************************************/
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return indexPath.section == 0 ? 160 : 260
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return section == 0 ? 1 : latestsRecipeList.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FeatureCell", for: indexPath) as! FeatureCell
                
                return cell
                
        } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! RecipeCell
                
                let recipe = latestsRecipeList[indexPath.row]
                let data = try? Data(contentsOf: recipe.image_url)
                
                cell.recipeNameLabel.text = recipe.title
                cell.recipeImageView.image = UIImage(data: data!)
                
                return cell
            }
        }
        //        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as? RecipeTableViewCell else {
        //            return UITableViewCell()
        //        }
        //
        //        let recipe = recipeList[indexPath.row]
        //        let data = try? Data(contentsOf: recipe.image_url)
        //        cell.imgView.image = UIImage(data: data!)
        //        cell.titleLabel.text = recipe.title
        
        //        return cell
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let featuredCell = cell as? FeatureCell
        featuredCell?.collectionView.delegate = self
        featuredCell?.collectionView.dataSource = self
        
        let collectionViewCellNib = UINib(nibName: "FeaturedCollectionCell", bundle: nil)
        featuredCell?.collectionView.register(collectionViewCellNib, forCellWithReuseIdentifier: "CollectionCell")
            featuredCell?.collectionView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! HeaderView
        headerView.headerLabel.text = sections[section]
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    //MARK: Prepare for Segue
    //
    /***************************************************************/
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "viewDetail"{
            
            let cell = sender as! RecipeCell
            //            let indexPath = tableView.indexPathForSelectedRow!
            let indexPath = tableView.indexPath(for: cell)!
            //            let recipeID = recipeList[indexPath.row].recipe_id
            //
            //            let finalUrl = baseDetailUrl + "&rId=\(recipeID)"
            
            //getRecipeData(url: finalUrl)
            
            recipe = latestsRecipeList[indexPath.row]
            
            let detailTableVC = segue.destination as! DetailTableViewController
            
            detailTableVC.recipe = recipe
            //detailTableVC.recipe = self.recipeWithDetails
            
            
        }
    }
    //MARK: Search Bar
    //
    /***************************************************************/
    
    //Search after pressing "Search" button
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("search button pressed")
        latestsRecipeList.removeAll()
        getRecipesQueryData(searchText: searchBar.text!)
        searchBar.resignFirstResponder()
        //        tableView.reloadData()
    }
    
}


//MARK: Collection view methods
//
/***************************************************************/

extension HomeTableViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        return featuredList.count
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! FeaturedCollectionCell
        
        if featuredList.count > 0 {
            let recipe = featuredList[indexPath.row]
            let data = try? Data(contentsOf: recipe.image_url)
            cell.featureRecipeImage.image = UIImage(data: data!)
            
            return cell
            
        } else {
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: 250, height: 120)
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 20, left: 16, bottom: 20, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailTableViewController
        
        vc?.recipe = featuredList[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
