//
//  LoginSignUpViewController.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-01.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginSignUpViewController: UIViewController {

    //UI View Properties
    @IBOutlet weak var logInButton: CustomButton!
    @IBOutlet weak var dontHaveAccountLabel: UILabel!
    @IBOutlet weak var signUpButton: CustomButton!
    @IBOutlet weak var titleLabel: CustomLabel!
    
    //First loading func
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpProperties()
    }
    
    //Sets up the UI elements
    func setUpProperties(){
        signUpButton.makeCustomWhiteButton()
        titleLabel.setSpacing(space: 1.75)
        
    }
    
    //Hides the status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if Auth.auth().currentUser != nil {
            self.performSegue(withIdentifier: "toHomeScreen", sender: nil)
        }
    }
}


