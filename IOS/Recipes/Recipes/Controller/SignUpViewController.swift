//
//  SignUpViewController.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-03.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var nameTextField: CustomTextField!
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: CustomTextField!
    
    //First loading function
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //Pops current VC in stack
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //Hides the status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func createAccountPressed(_ sender: Any) {
        
        SVProgressHUD.show()
        
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            
            if error != nil {
                print(error!.localizedDescription)
                SVProgressHUD.showError(withStatus: error!.localizedDescription)
                SVProgressHUD.dismiss(withDelay: 2)
                
            } else {
                print("registration succesfull")
                
                SVProgressHUD.dismiss()
                
                self.navigationController?.popToRootViewController(animated: true)
                
            }
            
        }
    }
    
}

//MARK: TextFields extensions
//Extension to dismiss the keyboard
extension SignUpViewController {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        navigationController?.navigationBar.isHidden = true
        topConstraint.constant = 40
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        topConstraint.constant = 80
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        navigationController?.navigationBar.isHidden = false
        topConstraint.constant = 80
        textField.resignFirstResponder()
        return true
    }
}
