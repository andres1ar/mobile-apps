//
//  FavoriteTableViewController.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-04.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit

class FavoriteTableViewController: UITableViewController {
    
    //fake data
    
    let recipes = ["pasta", "pasta", "pasta", "pasta"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let recipeCell = UINib(nibName: "RecipeCell", bundle: nil)
        tableView.register(recipeCell, forCellReuseIdentifier: "RecipeCell")

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
    
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recipes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath) as! RecipeCell
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
    }
    

}
