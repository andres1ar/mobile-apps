//
//  ProfileViewController.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-04.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class ProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func logOutPressed(_ sender: UIBarButtonItem) {
    print("logout button pressed")
        SVProgressHUD.show()
        do {
        try Auth.auth().signOut()
            print("logout succesfull")
            SVProgressHUD.dismiss()
            
            navigationController?.dismiss(animated: true, completion: nil)
            
        } catch {
            print("Error signing out")
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
