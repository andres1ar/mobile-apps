//
//  LoginViewController.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-02-03.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    //UI View Properties
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    //First Load function
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProperties()
    }
    
    //Sets up the UI elements
    func setupProperties(){
        navigationController?.navigationBar.layer.frame.origin.y = 22
    }
    
    //Pops current VC in stack
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //Hides the status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            
            if error != nil {
                print(error!.localizedDescription)
                SVProgressHUD.showError(withStatus: error!.localizedDescription)
                SVProgressHUD.dismiss(withDelay: 2)
                
            } else {
                print("login succesfull")
                
                SVProgressHUD.dismiss()
                
                self.navigationController?.popToRootViewController(animated: true)
                
            }
        }
    }
    
}

//Extension to dismiss the keyboard
extension LoginViewController {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        navigationController?.navigationBar.isHidden = true
        topConstraint.constant = 40
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        topConstraint.constant = 80
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        navigationController?.navigationBar.isHidden = false
        topConstraint.constant = 80
        textField.resignFirstResponder()
        return true
    }
}
