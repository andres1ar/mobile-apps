//
//  Recipe.swift
//  Recipes
//
//  Created by Andres Alderete on 2019-05-10.
//  Copyright © 2019 Andres Alderete. All rights reserved.
//

import Foundation

struct Recipe {
    var publisher: String
    var f2f_url: String
    var title: String
    var source_url: String
    var recipe_id: String
    var image_url: URL
    var social_rank: Double
    var publisher_url: String

}

//struct Recipe {
//    var publisher: String
//    var f2f_url: String
//    var ingredients: [String]
//    var title: String
//    var source_url: String
//    var recipe_id: String
//    var image_url: URL
//    var social_rank: Double
//    var publisher_url: String
//
//}
