package com.example.mobileapps.myrecipebook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mobileapps.myrecipebook.model.Recipes;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private List<Recipes> recipesList;
    private Context context;
    Activity activity;


    private static final String TAG = "ListAdapter";

    public ListAdapter() {
    }

    public ListAdapter(List<Recipes> recipesList, Context context, Activity activity) {
        this.recipesList = recipesList;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override

    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recipe_item, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, final int position) {

        final Recipes recipes = recipesList.get(position);
        holder.tvFirst.setText(recipes.getName());
        holder.tvSecond.setText(recipes.getDescription());
//        listViewHolder.imItem.setImageBitmap(recipes.getPictures().get(0));
        holder.imItem.setImageResource(R.drawable.shiba);

        //onClickListener
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DisplayRecipeActivity.class);
                //TODO put extra
//                Recipes r = recipesList.get(v.getId());
//                int recipeID = r.getId();
//
//                Log.v(TAG, "recipeID ****************"+recipeID);
//                intent.putExtra("recipeID",recipeID);
//                context.startActivity(intent);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//
//                activity.finish();
//                activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });
    }

    @Override
    public int getItemCount() {
        return recipesList.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        public TextView tvFirst, tvSecond;
        public ImageView imItem;

        public ListViewHolder(View itemView) {

            super(itemView);
            tvFirst = (TextView) itemView.findViewById(R.id.tvFirstLine);
            tvSecond = (TextView) itemView.findViewById(R.id.tvSecondLine);
            imItem = (ImageView) itemView.findViewById(R.id.ivIcon);

        }

        public void bindView(int position) {
            tvFirst.setText("item 1");
            tvSecond.setText("description");

        }


    }
}