package com.example.mobileapps.myrecipebook;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.mobileapps.myrecipebook.model.Recipes;
import com.example.mobileapps.myrecipebook.model.Steps;
import com.example.mobileapps.myrecipebook.services.RecipeService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DisplayRecipeActivity extends AppCompatActivity {


    private static final String TAG = "DisplayRecipeActivity";

    ListView lvSteps;
    ArrayAdapter<Steps> arrayAdapter;

    private List<Steps> stepsList;

    int recipeID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_recipe);
//        Intent intent = getIntent();
//        recipeID = intent.getIntExtra("recipeID", 2);

        lvSteps = (ListView) findViewById(R.id.lvSteps);

        stepsList = new ArrayList<Steps>();
        arrayAdapter = new ArrayAdapter<Steps>(this, android.R.layout.simple_list_item_1, stepsList);
        lvSteps.setAdapter(arrayAdapter);


//        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void loadSteps() {


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:7777/api.php/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RecipeService recipeService = retrofit.create(RecipeService.class);
Log.v(TAG, "recipeID "+ recipeID);
        Call<List<Steps>> call = recipeService.getSteps(2);

        call.enqueue(new Callback<List<Steps>>() {
            @Override
            public void onResponse(Call<List<Steps>> call, Response<List<Steps>> response) {
                if (!response.isSuccessful()) {
                    Log.e(TAG, "Code " + response.code());
                }
                List<Steps> newList = response.body();
                Log.v(TAG, "response.body: " + response.body());
                Log.v(TAG, "newlist: " + newList);

                Toast.makeText(DisplayRecipeActivity.this, "succeed", Toast.LENGTH_LONG).show();

                stepsList.clear();
                for (Steps s : newList) {
                    stepsList.add(s);
                }

                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Steps>> call, Throwable t) {
                Log.e(TAG, "failure on call");
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadSteps();
    }
}
