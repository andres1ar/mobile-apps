package com.example.mobileapps.myrecipebook.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Recipes {
    //    int id, prepTime, numServings;
//    String title, description, foodCategory;
//    String[] ingredients;
//    ArrayList<Bitmap> pictures;

    @SerializedName("id")
    private int id;

    @SerializedName("ownerid")
    private int ownerid;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("category")
    private String category;

    @SerializedName("preparationTime")
    private int preparationTime;

    @SerializedName("numberofservings")
    private int numberofservings;

    public Recipes() {
    }

    public Recipes(int id, int ownerid, String name, String description, String category, int preparationTime, int numberofservings) {
        this.id = id;
        this.ownerid = ownerid;
        this.name = name;
        this.description = description;
        this.category = category;
        this.preparationTime = preparationTime;
        this.numberofservings = numberofservings;
    }

    public int getId() {
        return id;
    }

    public int getOwnerid() {
        return ownerid;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public int getPreparationTime() {
        return preparationTime;
    }

    public int getNumberofservings() {
        return numberofservings;
    }

    @Override
    public String toString() {
        return "Recipes{" +
                "id=" + id +
                ", ownerid=" + ownerid +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", preparationTime=" + preparationTime +
                ", numberofservings=" + numberofservings +
                '}';
    }

    //    public Recipes() {
//    }
//
//    public Recipes(String title, String description) {
//        this.title = title;
//        this.description = description;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public int getPrepTime() {
//        return prepTime;
//    }
//
//    public int getNumServings() {
//        return numServings;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public String getFoodCategory() {
//        return foodCategory;
//    }
//
//    public String[] getIngredients() {
//        return ingredients;
//    }
//
//    public ArrayList<Bitmap> getPictures() {
//        return pictures;
//    }
}
