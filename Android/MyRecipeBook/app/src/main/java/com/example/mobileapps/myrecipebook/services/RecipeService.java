package com.example.mobileapps.myrecipebook.services;

import com.example.mobileapps.myrecipebook.model.Recipes;
import com.example.mobileapps.myrecipebook.model.Steps;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RecipeService {

    @GET("recipes")
    Call<List<Recipes>> getRecipes();

    @GET("steps/{recipeid}")
    Call<List<Steps>> getSteps(@Path("recipeid") int recipeid);

    @POST("recipes")
    Call<List<Recipes>> createRecipe(@Body Recipes recipe);

}
