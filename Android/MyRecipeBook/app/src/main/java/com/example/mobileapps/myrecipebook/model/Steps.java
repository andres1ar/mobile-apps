package com.example.mobileapps.myrecipebook.model;

import com.google.gson.annotations.SerializedName;

public class Steps {
    @SerializedName("id")
    int id;
    @SerializedName("step")
    String step;

    public Steps(int id, String step) {
        this.id = id;
        this.step = step;
    }

    public int getId() {
        return id;
    }

    public String getStep() {
        return step;
    }

    @Override
    public String toString() {
        return "Steps{" +
                "step='" + step + '\'' +
                '}';
    }
}
