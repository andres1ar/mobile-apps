package com.example.mobileapps.myrecipebook;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigationView = (BottomNavigationView) findViewById(R.id.navigation);

        final HomeFragment homeFragment = new HomeFragment();
        final SaveRecipesFragment saveRecipesFragment = new SaveRecipesFragment();
        final CreateRecipeFragment createRecipeFragment = new CreateRecipeFragment();
        final MyAccountFragment myAccountFragment = new MyAccountFragment();

        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navigation_home:
                        setFragment(homeFragment);
                        return true;
                    case R.id.navigation_saved_recipes:
                        setFragment(saveRecipesFragment);
                        return true;
                    case R.id.navigation_create_recipe:
                        setFragment(createRecipeFragment);
                        return true;
                    case R.id.navigation_my_account:
                        setFragment(myAccountFragment);
                        return true;
                }
                return false;
            }
        });
        navigationView.setSelectedItemId(R.id.navigation_home);

    }
    private void setFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame,fragment);
        fragmentTransaction.commit();
    }

}
