package com.example.mobileapps.notepad;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        etPassword = (EditText) findViewById(R.id.etPassword);
    }

    public void onLoginClick(View view) {

        String passEntered = etPassword.getText().toString();
        String passStored = sharedPreferences.getString("prefUserPassword","");
        if (passEntered.equals(passStored)) {
            Intent intent = new Intent(this, NotepadActivity.class);
            startActivity(intent);
        }else {
            //TODO: use a dialog box instead
            Toast.makeText(this,"Password invalid", Toast.LENGTH_LONG).show();
        }
    }
}
