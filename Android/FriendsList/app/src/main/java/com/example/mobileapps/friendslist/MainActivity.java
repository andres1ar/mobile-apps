package com.example.mobileapps.friendslist;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    ListView lvFriends;
    ArrayList<String> friendsList;

    ArrayAdapter<String> friendsAdapter;

    public static final String FILE_NAME = "friends.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvFriends = (ListView) findViewById(R.id.lvFriends);

        friendsList = new ArrayList<String>();

        friendsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, friendsList);

        lvFriends.setAdapter(friendsAdapter);

        lvFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String nameClicked = (String) parent.getItemAtPosition(position);
                showModifyFriendDialog(nameClicked, position);
            }
        });

        lvFriends.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
                String nameClicked = (String) parent.getItemAtPosition(position);
                showDeleteFriendDialog(nameClicked);
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add:
                showAddFriendDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAddFriendDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add friend?");
        //setup additional edit text
        final EditText etFriendName = new EditText(this);
        etFriendName.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(etFriendName);

        //Positive button
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = etFriendName.getText().toString();
                friendsList.add(name);
                friendsAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, name + " added", Toast.LENGTH_LONG).show();
            }
        });

        //Negative button
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void showModifyFriendDialog(final String nameClicked, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Modify friend?");
        //setup additional edit text
        final EditText etFriendName = new EditText(this);
        etFriendName.setInputType(InputType.TYPE_CLASS_TEXT);
        etFriendName.setText(nameClicked);
        builder.setView(etFriendName);

        //Positive button
        builder.setPositiveButton("Modify", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                friendsList.remove(nameClicked);
                String name = etFriendName.getText().toString();
                friendsList.add(position, name);
                friendsAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, name + " modified", Toast.LENGTH_LONG).show();
            }
        });

        //Negative button
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void showDeleteFriendDialog(final String nameClicked) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Delete friend?");

        //Positive button
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                friendsList.remove(nameClicked);
                friendsAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, nameClicked + " deleted", Toast.LENGTH_LONG).show();
            }
        });

        //Negative button
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void saveFriendsToFile() {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);

            for (String friend : friendsList) {
                fileOutputStream.write(friend.getBytes());
                String jumpLine = "\n";
                fileOutputStream.write(jumpLine.getBytes());

            }

        } catch (IOException ex) {
            Toast.makeText(this, "Error saving text to file", Toast.LENGTH_LONG).show();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();

                } catch (IOException ex) {
                    Toast.makeText(this, "Error saving text to file", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void loadFriendsFromFile() {
        FileInputStream fileInputStream = null;
        Scanner fileInput = null;
        try {
            fileInputStream = openFileInput(FILE_NAME);
            fileInput = new Scanner(fileInputStream);
            fileInput.useDelimiter("\n");
            while (fileInput.hasNext()) {

                String text = fileInput.next();
                friendsList.add(text);
            }

        } catch (FileNotFoundException ex) {
            //ignore if file doesnt exist yet

        } catch (IOException ex) {
            Toast.makeText(this, "Error loading text from file", Toast.LENGTH_LONG).show();
        } finally {
            if (fileInput != null) {
                fileInput.close();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        loadFriendsFromFile();
    }


    @Override
    public void onStop() {
        super.onStop();
        saveFriendsToFile();
    }
}
