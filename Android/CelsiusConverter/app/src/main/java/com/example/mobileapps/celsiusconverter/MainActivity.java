package com.example.mobileapps.celsiusconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {
    int first;
    int second;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText txtIn = findViewById(R.id.txtIn);
        EditText txtOut = findViewById(R.id.txtOut);

        Button btnConvert = findViewById(R.id.btnConvert);

        RadioGroup rb = (RadioGroup) findViewById(R.id.radio_group1);
        rb.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButton:
                        first = 1;
                        break;
                    case R.id.radioButton2:
                        first = 2;
                        break;
                    case R.id.radioButton3:
                        first = 3;
                        break;
                }
            }

        });

        RadioGroup rb2 = (RadioGroup) findViewById(R.id.radio_group2);
        rb2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButton:
                        second = 1;
                        break;
                    case R.id.radioButton5:
                        second = 2;
                        break;
                    case R.id.radioButton6:
                        second = 3;
                        break;
                }
            }

        });

        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertValue(v,first,second);
            }
        });
    }

    public void convertValue(View view, int first, int second) {
        if (first==1&&second==1){

        }
    }

    private double celciusToF(double temp) {
        return temp * 32;
    }

    private double faToCelcius(double temp) {
        return temp / 32;
    }

    private double celciusToK(double temp) {
        return temp * 273.15;
    }

    private double kelToCelcius(double temp) {
        return temp / 273.15;
    }

    private double fahToKel(double temp) {
        return temp * 255.92;
    }

    private double kelToFah(double temp) {
        return temp / 255.92;
    }

}
